// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayPickableObject.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"






// Sets default values
AUPlayPickableObject::AUPlayPickableObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	CollisionComponent->InitSphereRadius(50.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	SetRootComponent(CollisionComponent);

	bReplicates = true;


}

// Called when the game starts or when spawned
void AUPlayPickableObject::BeginPlay()
{
	Super::BeginPlay();
	check(CollisionComponent);

	OnActorBeginOverlap.AddDynamic(this, &AUPlayPickableObject::OnOverlap);

	GenerateRotationYaw();

}

// Called every frame
void AUPlayPickableObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	AddActorLocalRotation(FRotator(0.0f, RotationYaw, 0.0f));
}





void AUPlayPickableObject::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);


	const auto Pawn = Cast<APawn>(OtherActor);
	if (GivePickUpTo(Pawn))
	{
		PickUpWasTaken();
	}
		

}



bool AUPlayPickableObject::GivePickUpTo(APawn* PlayerPawn)
{
	return false;
}

bool AUPlayPickableObject::CouldBeTaken() const
{
	return GetWorldTimerManager().IsTimerActive(RespawnTimerHandle);
	
}

void AUPlayPickableObject::GenerateRotationYaw()
{


}


void AUPlayPickableObject::OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor)
{
	if ((OtherActor != this) && (OtherActor != nullptr))
	{
		MyCharacter = Cast<AUplayCharacter>(OtherActor);

		PickUpWasTaken();
		
		/*if (MyCharacter->HealthComponent->GetHealth() <= 100.0f)
		{
			
			MyCharacter->HealthComponent->UpdateHealth(33.f);
			UE_LOG(LogBasePickup, Display, TEXT("Health was taken"));
			
		}*/
		


	}	
	
}

void AUPlayPickableObject::PickUpWasTaken()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(false, true);
	}
	
	
	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &AUPlayPickableObject::Respawn, RespawnTime);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickupTakenSound, GetActorLocation());

}


void AUPlayPickableObject::Respawn()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(true, true);
	}
}

