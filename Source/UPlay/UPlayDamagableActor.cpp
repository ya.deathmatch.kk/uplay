// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayDamagableActor.h"
#include "UPlayHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TextRenderComponent.h"

// Sets default values
AUPlayDamagableActor::AUPlayDamagableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(GetRootComponent());


	BoxCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollission"));
	BoxCollisionComponent->SetupAttachment(GetRootComponent());
		

	HealthComponent = CreateDefaultSubobject<UUPlayHealthComponent>(TEXT("HealthComponent"));


	HealthTextComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("HealthTextComponent"));
	HealthTextComponent->SetupAttachment(GetRootComponent());


	

	TSubclassOf<UDamageType> DamageType;  // Default Damage
	/*UGameplayStatics::ApplyDamage(this, Damage, GetController(), this, DamageType);*/
}

// Called when the game starts or when spawned
void AUPlayDamagableActor::BeginPlay()
{
	Super::BeginPlay();
	/*HealthComponent->OnHealthChanged.AddDynamic(this, &AUPlayDamagableActor::OnHealthChanged);*/
	check(HealthComponent);
	check(HealthTextComponent);
}

//void AUPlayDamagableActor::OnHealthChanged(UUPlayHealthComponent* HealthComp, float Health, float DamageAmount, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
//{
//
//	
//	if (Health <= 0.0f)
//	{
//		this->Destroy();
//	}
//	UE_LOG(LogTemp, Warning, TEXT("The Player's Current Health is: %f"), Health);
//
//}
//
//void AUPlayDamagableActor::DamagePlayerCharacter()
//{
//	UGameplayStatics::ApplyDamage(this, 20.0f, GetInstigatorController(), this, GenericDamageType);
//
//}

// Called every frame
void AUPlayDamagableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	const auto Health = HealthComponent->GetHealth();
	
	HealthTextComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));
	
}



