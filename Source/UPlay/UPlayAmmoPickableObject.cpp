// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayAmmoPickableObject.h"
#include "UPlayHealthComponent.h"
#include "UPlayWeaponComponent.h"
#include "UPlay/Public/UPlayUtils.h"
#include "UPlay/Public/UPlayBaseWeapon.h"



DEFINE_LOG_CATEGORY_STATIC(LogAmmoPickup, All, All);

bool AUPlayAmmoPickableObject::GivePickUpTo(APawn* PlayerPawn)
{
	const auto HealthComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayHealthComponent>(PlayerPawn);
	if (!HealthComponent) return false;

	const auto WeaponComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayWeaponComponent>(PlayerPawn);
	if (!WeaponComponent) return false;

	/*CurrentAmmo.Bullets = FMath::Clamp(8, 0, DefaultAmmo.Bullets + 1);*/

	return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);

	
}

//void AUPlayAmmoPickableObject::OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor)
//{
//	Super::OnOverlap(MyOverlappedActor, OtherActor);
//	const auto Pawn = Cast<APawn>(GetOwner());
//	GivePickUpTo(Pawn);
//	
//}
