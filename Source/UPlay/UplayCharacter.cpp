// Fill out your copyright notice in the Description page of Project Settings.


#include "UplayCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "UPlayHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"
#include "UPlayBaseWeapon.h"
#include "UPlayWeaponComponent.h"
#include "UPlayProjectile.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"



DEFINE_LOG_CATEGORY_STATIC(LogCharacter, All, All)


// Sets default values
AUplayCharacter::AUplayCharacter(const FObjectInitializer& ObjInit) :Super(ObjInit) /*: Super(ObjInit.SetDefaultSubobjectClass<UPlayCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))*/
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//��������� ������ � ������� ������
	bReplicates = true;
	
	HealthComponent = CreateDefaultSubobject<UUPlayHealthComponent>(TEXT("HealthComponent"));
	WeaponComponent = CreateDefaultSubobject<UUPlayWeaponComponent>("WeaponComponent");
	WeaponComponent->SetIsReplicated(true);
	HealthComponent->SetIsReplicated(true);
	
	
}







// Called when the game starts or when spawned
void AUplayCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	OnHealthChanged(HealthComponent->GetHealth(), 0.0f);
	HealthComponent->OnDeath.AddUObject(this, &AUplayCharacter::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &AUplayCharacter::OnHealthChanged);
}




// Called every frame
void AUplayCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	

	if (bZoomingIn)
	{
		ZoomFactor += DeltaTime / 0.5f;         //Zoom in over half a second
	}
	else
	{
		ZoomFactor -= DeltaTime / 0.25f;        //Zoom out over a quarter of a second
	}
	ZoomFactor = FMath::Clamp<float>(ZoomFactor, 0.0f, 1.0f);
	//Blend our camera's FOV and our SpringArm's length based on ZoomFactor
	
	


	/*const auto Health = HealthComponent->GetHealth();
	if (HealthComponent->GetHealth() <= 0)
	{
		OnDeath();
		
		PlayAnimMontage(DeathAnim);
		
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
	}*/

	



}

// Called to bind functionality to input





void AUplayCharacter::ZoomIn()
{
	bZoomingIn = true;
}

void AUplayCharacter::ZoomOut()
{
	bZoomingIn = false;
}

float AUplayCharacter::GetMovementDirection() const
{
	if (GetVelocity().IsZero()) return 0.0f; 
	const auto VelocityNormal = GetVelocity().GetSafeNormal();
	const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
	const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);
	const auto Degrees = FMath::RadiansToDegrees(AngleBetween);
	return CrossProduct.IsZero() ? Degrees: Degrees * FMath::Sign(CrossProduct.Z);
	//return false;
}



//void AUplayCharacter::Fire()
//{
//	// Attempt to fire a projectile.
//	if (ProjectileClass)
//	{
//		// Get the camera transform.
//		FVector CameraLocation;
//		FRotator CameraRotation;
//		GetActorEyesViewPoint(CameraLocation, CameraRotation);
//
//		// Set MuzzleOffset to spawn projectiles slightly in front of the camera.
//		MuzzleOffset.Set(100.0f, 0.0f, 0.0f);
//
//		// Transform MuzzleOffset from camera space to world space.
//		FVector MuzzleLocation = CameraLocation + FTransform(CameraRotation).TransformVector(MuzzleOffset);
//
//		// Skew the aim to be slightly upwards.
//		FRotator MuzzleRotation = CameraRotation;
//		MuzzleRotation.Pitch += 0.05f;
//
//		UWorld* World = GetWorld();
//		if (World)
//		{
//			FActorSpawnParameters SpawnParams;
//			SpawnParams.Owner = this;
//			SpawnParams.Instigator = GetInstigator();
//
//			// Spawn the projectile at the muzzle.
//			AUplayProjectile* Projectile = World->SpawnActor<AUplayProjectile>(ProjectileClass, MuzzleLocation, MuzzleRotation, SpawnParams);
//			if (Projectile)
//			{
//				// Set the projectile's initial trajectory.
//				FVector LaunchDirection = MuzzleRotation.Vector();
//				Projectile->FireInDirection(LaunchDirection);
//			}
//		}
//	}
//}

void AUplayCharacter::OnDeath()
{

	UE_LOG(LogCharacter, Display, TEXT("Player %s is dead"), *GetName());
	GetCharacterMovement()->DisableMovement();
	PlayAnimMontage(DeathAnim);
	SetLifeSpan(LifeSpanOnDeath);
	
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	WeaponComponent->StopFire();
	

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetSimulatePhysics(true);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
	
}

void AUplayCharacter::SetPlayerColor(const FLinearColor& Color)
{

	const auto MateialInst1 = GetMesh()->CreateAndSetMaterialInstanceDynamic(1);
	const auto MateialInst0 = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
	

	MateialInst1->SetVectorParameterValue(MaterialColorName, Color);
	MateialInst0->SetVectorParameterValue(MaterialColorName, Color);
}



void AUplayCharacter::OnHealthChanged(float Health, float HealthDelta)
{
	
}