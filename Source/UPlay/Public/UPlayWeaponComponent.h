// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UPlayBaseWeapon.h"
#include "UPlayCoreTypes.h"
#include "UPlayWeaponComponent.generated.h"



class AUPlayBaseWeapon;
class UUPlayWeaponComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UPLAY_API UUPlayWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUPlayWeaponComponent();

	virtual void StartFire();
	virtual void StopFire();
	
	
	bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;


	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TArray<TSubclassOf<AUPlayBaseWeapon>> WeaponClasses;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponEquipSocketName = "WeaponSocket";


	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponArmorySocketName = "ArmorySocket";


	UPROPERTY()
	AUPlayBaseWeapon* CurrentWeapon = nullptr;
	
	UPROPERTY()
	TArray<AUPlayBaseWeapon*> Weapons;

	int32 CurrentWeaponIndex = 0;
public:	
	//// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SpawnWeapons();
	void AttachWeaponToSocket(AUPlayBaseWeapon* Weapon, USceneComponent* SceneComponent, FName& SocketName);
	void EquipWeapon(int32 WeaponIndex);
	virtual void NextWeapon();

	bool TryToAddAmmo(TSubclassOf<AUPlayBaseWeapon> WeaponType, int32 ClipsAmount);
	bool NeedAmmo(TSubclassOf<AUPlayBaseWeapon> WeaponType);
	void OnClipEmpty(AUPlayBaseWeapon* AmmoEmptyWeapon);


};
