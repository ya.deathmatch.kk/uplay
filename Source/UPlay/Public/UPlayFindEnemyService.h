// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "UPlayFindEnemyService.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API UUPlayFindEnemyService : public UBTService
{
	GENERATED_BODY()



public:
	UUPlayFindEnemyService();

protected: 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
