// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UPlay/Public/UPlayCoreTypes.h"
#include "UPlayBaseWidget.h"
#include "UPlayPauseWidget.generated.h"

/**
 * 
 */

class UButton;




UCLASS()
class UPLAY_API UUPlayPauseWidget : public UUPlayBaseWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeOnInitialized() override;

protected:

	UPROPERTY(meta = (BindWidget))
		UButton* ClearPauseButton;

private:

	UFUNCTION()
	void OnClearPause();


};
