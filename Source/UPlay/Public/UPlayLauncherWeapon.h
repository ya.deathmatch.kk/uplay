// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPlayBaseWeapon.h"
#include "UplayGrenadeProjectiler.h"
#include "UPlayLauncherWeapon.generated.h"

/**
 * 
 * 
 * 
 */


class AUplayProjectile;
class AUplayGrenadeProjectiler;
class USoundCue;

UCLASS()
class UPLAY_API AUPlayLauncherWeapon : public AUPlayBaseWeapon
{
	GENERATED_BODY()
	
	
public:
	AUPlayLauncherWeapon();
	virtual void StartFire() override;
	FName MuzzleOffset = "MuzzleSocket";
	
protected:
	/*UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<AUplayProjectile> ProjectileClass;*/

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<AUPlayGrenadeProjectiler> GrenadeProjectile;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float BulletSpread = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float TimeBetweenShots = 3.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* NoAmmoSound;

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector MuzzleOffsets;*/

	////// Particle used when the projectile impacts against another object and explodes.
	UPROPERTY(EditAnywhere, Category = "VFX")
		class UParticleSystem* ExplosionEffect;
	
	virtual void MakeShot() override;
	
	FTimerHandle ShotTimerHandle;
};
