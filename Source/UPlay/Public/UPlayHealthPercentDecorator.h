// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "UPlayHealthPercentDecorator.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API UUPlayHealthPercentDecorator : public UBTDecorator
{
	GENERATED_BODY()
public:
	UUPlayHealthPercentDecorator();


	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Pickup")
	float HealthPercent = 0.6f;



	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
