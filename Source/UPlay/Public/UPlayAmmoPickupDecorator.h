// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "UPlayAmmoPickupDecorator.generated.h"

/**
 * 
 */
class AUPlayBaseWeapon;



UCLASS()
class UPLAY_API UUPlayAmmoPickupDecorator : public UBTDecorator
{
	GENERATED_BODY()

public:
	UUPlayAmmoPickupDecorator();
	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Pickup")
	TSubclassOf<AUPlayBaseWeapon> WeaponType;

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

};
