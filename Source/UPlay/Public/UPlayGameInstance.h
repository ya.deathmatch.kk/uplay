// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "UPlay/Public/UPlayCoreTypes.h"
#include "UPlayGameInstance.generated.h"

/**
 * 
 */

class USoundClass;
UCLASS()
class UPLAY_API UUPlayGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	

	FLevelData GetStartupLevel() const { return StartupLevel; }
	void SetStartupLEvel(const FLevelData& Data) { StartupLevel = Data; }

	TArray<FLevelData> GetLevelsData() const { return LevelsData; }	
	FName GetMenuLevelName() const { return MenuLevelName; }


	void ToggleVolume();


protected:

	UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Level name must be unique!"))
		TArray<FLevelData> LevelsData;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
		FName MenuLevelName = NAME_None;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
		USoundClass* MasterSoundClass;

private:

	FLevelData StartupLevel;
	
	
};
