// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UPlayCoreTypes.h"
#include "UPlayWeaponFXComponent.generated.h"


class UNiagaraSystem;
class UPhysicalMaterial;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UPLAY_API UUPlayWeaponFXComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUPlayWeaponFXComponent();


	void PlayImpactFX(const FHitResult& Hit);



protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FImpactData DefaultImpactData;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;
	
	////// Particle used when the projectile impacts against another object and explodes.
	UPROPERTY(EditAnywhere, Category = "VFX")
		class UParticleSystem* ExplosionEffect;
		
};
