// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UPlay/Public/UPlayCoreTypes.h"
#include "UPlayBaseWidget.h"
#include "UPlayMenuWidget.generated.h"

/**
 * 
 */


class UButton;
class USoundCue;
class UHorizontalBox;
class UUPlayGameInstance;
class UUPlayLevelItemWidget;


UCLASS()
class UPLAY_API UUPlayMenuWidget : public UUPlayBaseWidget
{
	GENERATED_BODY()


public:
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* CreateGameButton;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* JoinGameButton;

protected:
	UPROPERTY(meta = (BindWidget))
		UButton* StartGameButton;

	UPROPERTY(meta = (BindWidget))
		UButton* QuitGameButton;

	

	UPROPERTY(meta = (BindWidget))
		UButton* GoToCreateGameButton;

	UPROPERTY(meta = (BindWidget))
		UButton* GoToJoinGameButton;

	UPROPERTY(meta = (BindWidget))
		UButton* BacktoMenuButtonCreate;

	UPROPERTY(meta = (BindWidget))
		UButton* BacktoMenuButtonJoin;

	UPROPERTY(meta = (BindWidget))
		UHorizontalBox* LevelItemBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		TSubclassOf<UUserWidget> LEvelItemWidgetClass;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
		UWidgetAnimation* HideAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* StartGameSound;

	virtual void NativeOnInitialized() override;

	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
private:


	UPROPERTY()
		TArray<UUPlayLevelItemWidget*> LevelItemWidgets;




	UFUNCTION()
		void OnStartGame();

	UFUNCTION()
		void OnQuitGame();

	void InitLevelItems();
	void OnLevelSelected(const FLevelData& Data);

	UFUNCTION()
		void OnCreateServer();

	UFUNCTION()
		void OnJoinServer();


	
	UUPlayGameInstance* GetUplayGameInstance() const;
};
