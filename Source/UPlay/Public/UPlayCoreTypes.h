#pragma once


#include "UPlayCoreTypes.generated.h"



//DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChangedSignature, float);
DECLARE_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_MULTICAST_DELEGATE(FOnClipEmptySignature);







class AUPlayBaseWeapon;


USTRUCT(BlueprintType)			//Creating Ammo through making struct
struct FAmmoData
{

	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
		int32 Bullets;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon", meta = (EditCondition = "!Infinite"))
		int32 Clips;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
		bool Infinite;

};



USTRUCT(BlueprintType)			//Creating Ammo through making struct
struct FWeaponData
{

	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<AUPlayBaseWeapon> WeaponClass;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon", meta = (EditCondition = "!Infinite"))
		UAnimMontage* ReloadAnimMontage;
		

};



USTRUCT(BlueprintType)			//Creating Ammo through making struct
struct FGameData
{

	GENERATED_USTRUCT_BODY()



		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "1", ClampMax = "100"))
		int32 PlayersNum = 2;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "1", ClampMax = "10"))
		int32 RoundsNum = 4;


		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "3", ClampMax = "300"))
		int32 RoundTime = 10; // In seconds



		UPROPERTY(EditAnyWhere, BlueprintReadWrite)
			FLinearColor DefaultTeamColor = FLinearColor::Red;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite)
			TArray<FLinearColor>  TeamColors;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "3", ClampMax = "20"))
			int32 RespawnTime = 5;
};


UENUM(BlueprintType)
enum class EUplayMatchState : uint8
{
	WaitingToStart = 0,
	InProgress,
	Pause, 
	GameOver
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnMatchStateChangedSignature, EUplayMatchState);



USTRUCT(BlueprintType)
struct FLevelData
{

	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game")
			FName LevelName = NAME_None;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game")
			FName LevelDisplayName = NAME_None;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game")
			UTexture2D* LevelThumb;


};


DECLARE_MULTICAST_DELEGATE_OneParam(FOnLevelSelectedSignature, const FLevelData&)


class UNiagaraSystem;
class USoundCue;

USTRUCT(BlueprintType)
struct FDecalData
{

	GENERATED_USTRUCT_BODY()


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UMaterialInterface* Material;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FVector Size = FVector(10.0f);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	float LifeTime = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	float FadeOutTime = 0.7f;

};


USTRUCT(BlueprintType)
struct  FImpactData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UNiagaraSystem* NiagaraEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FDecalData DecalData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	USoundCue* Sound;



};