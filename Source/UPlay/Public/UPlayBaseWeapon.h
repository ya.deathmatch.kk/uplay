 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UPlayCoreTypes.h"
#include "UPlayBaseWeapon.generated.h"


class USkeletalMeshComponent;

class USoundCue;
class UNiagaraSystem;
class UNiagaraComponent;




UCLASS()
class UPLAY_API AUPlayBaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUPlayBaseWeapon();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
	FName MuzzleSocketName = "MuzzleSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* FireSound;
	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UNiagaraSystem* MuzzleFX;
	
	


	void DecreaseAmmo();
	
	bool IsClipEmpty() const;
	void ChangeClip();
	void LogAmmo();
	

public:	
	
	



	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float TraceMaxDistance = 5000.0f;


	


	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
	FAmmoData DefaultAmmo{ 30, 10, false };  // Declare Amount of ammo

	FAmmoData GetAmmoData() const { return CurrentAmmo; }
	

	bool TryToAddAmmo(int32 BulletsAmount);
	
	

	
	virtual void MakeShot();
	virtual void StartFire();
	virtual void StopFire();
	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const;
	bool IsAmmoEmpty() const;
	bool IsAmmoFull() const;

	/*void Fire();*/
	
	/*APlayerController* GetPlayerController() const;*/
	bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;
	FVector  GetMuzzleWorldLocation() const;
	FRotator  GetMuzzleWorldRotation() const;
	void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd);
	
	UNiagaraComponent* SpawnMuzzleFX();

	FAmmoData CurrentAmmo; // Current Ammo
};
