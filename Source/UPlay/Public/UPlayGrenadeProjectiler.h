// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"
#include "UPlayGrenadeProjectiler.generated.h"




class USphereComponent;
class UProjectileMovementComponent;
class UUPlayWeaponFXComponent;


UCLASS()
class UPLAY_API AUPlayGrenadeProjectiler : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUPlayGrenadeProjectiler();


	UPROPERTY(VisibleAnywhere, Category = "Weapon")
	USphereComponent* CollisionComponent;

	//// Projectile movement component.
	UPROPERTY(VisibleAnywhere, Category = "Weapon")
	UProjectileMovementComponent* MovementComponent;
	
	
	void SetShotDirection(const FVector& Direction);
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float TimeBetweenShots = 3.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float DamageRadius = 200.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float DamageAmount = 50.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	bool DoFullDamage = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float LifeSeconds = 5.0f;

	UPROPERTY(VisibleAnyWhere, Category = "VFX")
	UUPlayWeaponFXComponent* WeaponFXComponent;

	////// Particle used when the projectile impacts against another object and explodes.
	UPROPERTY(EditAnywhere, Category = "VFX")
		class UParticleSystem* ExplosionEffect;

public:	
	//// Called every frame
	virtual void Tick(float DeltaTime) override;


	
private:

	FVector ShotDirection;

	UFUNCTION()
	void OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	AController* GetController() const;
	
	
};
