// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "UPlayBTTaskNode.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API UUPlayBTTaskNode : public UBTTaskNode
{
	GENERATED_BODY()
public:
		UUPlayBTTaskNode();
		virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
		
		
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
			float Radius = 1000.0f;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
			FBlackboardKeySelector AimLocationKey;

	
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
			bool SelfCenter = true;


		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI", meta = (EditCondition = "!SelfCenter"))
			FBlackboardKeySelector CenterActorKey;;




};
