// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "UPlayChangeWeaponService.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API UUPlayChangeWeaponService : public UBTService
{
	GENERATED_BODY()


public:
	UUPlayChangeWeaponService();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI", meta = (ClampMin = "0.0", ClampMax = "1.0"))
		float Probability = 0.5f;


	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	

};
