// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPlayBaseWeapon.h"
#include "UPlayRifleWeapon.generated.h"

/**
 * 
 */

class UAudioComponent;
class UNiagaraComponent;
class UNiagaraSystem;
class UUPlayWeaponFXComponent;

UCLASS()
class UPLAY_API AUPlayRifleWeapon : public AUPlayBaseWeapon
{
	GENERATED_BODY()
	
public:

	AUPlayRifleWeapon();


	virtual void StartFire() override;
	virtual void StopFire() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float TimeBetweenShots = 0.1f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float BulletSpread = 0.5f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float DamageAmount = 1.0f;
	

protected:


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UNiagaraSystem* TraceFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FString TraceTargetName = "TraceTarget";

	UPROPERTY(VisibleAnyWhere, Category = "VFX")
	UUPlayWeaponFXComponent* WeaponFXComponent;

	////// Particle used when the projectile impacts against another object and explodes.
	UPROPERTY(EditAnywhere, Category = "VFX")
		class UParticleSystem* FireFlash;

	virtual void BeginPlay() override;

	virtual void MakeShot() override;
	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const override;

	
	


private:
	FTimerHandle ShotTimerHandle;

	UPROPERTY()
	UAudioComponent* FireAudioComponent;

	UPROPERTY()
	UNiagaraComponent* MuzzleFXComponent;

	void MakeDamage(const FHitResult& HitResult);

	void InitFX();
	void SetFXActive(bool IsActive);
	void SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);

	AController* GetController() const;
};
