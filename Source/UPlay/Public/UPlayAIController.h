// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "UPlayAIController.generated.h"

/**
 * 
 */

class UUPlayAIPerceptionComponent;
class UUPlayRespawnComponent;


UCLASS()
class UPLAY_API AUPlayAIController : public AAIController
{
	GENERATED_BODY()
public:
		AUPlayAIController();




protected:
	


	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Components")
	UUPlayAIPerceptionComponent* UPlayAIPerceptionComponent;
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Components")
	UUPlayRespawnComponent* RespawnComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		FName FocusOnKeyName = "EnemyActor";


	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaTime) override;


private:
	AActor* GetFocusOnActor() const;


};
