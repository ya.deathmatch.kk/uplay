// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "UPlayBaseWeapon.h"
#include "UPlay/UPlayCharacter.h"
#include "UPlayAmmo.generated.h"



class UPlayBaseWeapon;

UCLASS()
class UPLAY_API AUPlayAmmo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUPlayAmmo();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnyWhere, Category = "Pickup")
	USphereComponent* CollisionComponent;



	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Pickup")
	float RespawnTime = 5.0f;


	UPROPERTY(EditAnyWhere)
	AUplayCharacter* MyCharacter;

	UPROPERTY(EditAnyWhere)
	AUPlayBaseWeapon* MyAmmo;


	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp", meta = (ClampMin = "1.0", ClampMax = "10.0"))
	int32 BulletsAmount = 10;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	void OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor);


	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp")
	TSubclassOf<AUPlayBaseWeapon> WeaponType;


private:
	virtual bool GivePickUpTo(APawn* PlayerPawn);
	

	void PickUpWasTaken();
	void Respawn();
};
