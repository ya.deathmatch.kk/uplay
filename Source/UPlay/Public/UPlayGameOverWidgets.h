// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UPlayCoreTypes.h"
#include "UPlayBaseWidget.h"
#include "UPlayGameOverWidgets.generated.h"

/**
 * 
 */

class UVerticalBox;
class UButton;


UCLASS()
class UPLAY_API UUPlayGameOverWidgets : public UUPlayBaseWidget
{
	GENERATED_BODY()
	

public:
	/*virtual bool Initialize() override;*/





protected:

	UPROPERTY(meta = (BindWidget))
		UVerticalBox* PlayerStatBox;


	UPROPERTY(meta = (BindWidget))
		UButton* ResetLevelButton;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		TSubclassOf<UUserWidget> PlayerStatRowWidgetClass;
	
	
	virtual void NativeOnInitialized() override;

private:
	void OnMatchStateChange(EUplayMatchState State);
	void UpdatePlayerStat();

	UFUNCTION()
	void OnResetLevel();
};
