// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPlayWeaponComponent.h"
#include "UPlayAIWeaponComponent.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API UUPlayAIWeaponComponent : public UUPlayWeaponComponent
{
	GENERATED_BODY()


public:
	virtual void StartFire() override;
	/*virtual void StopFire() override;*/
	virtual void NextWeapon() override;

	
};
