// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPlay/UPlayCharacter.h"
#include "UplayAICharacter.generated.h"

/**
 * 
 */

class UBehaviorTree;
class UWidgetComponent;
class UTextRenderComponent;

UCLASS()
class UPLAY_API AUplayAICharacter : public AUplayCharacter
{
	GENERATED_BODY()
	

public:

	AUplayAICharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
		UBehaviorTree* BehaviorTreeAsset;
		

	virtual void Tick(float DeltaTime) override;
	

protected:
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Components")
		UWidgetComponent* HealthWidgetComponent;
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Components")
		UTextRenderComponent* HealthTextComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
		float HealthVisibilityDistance = 1000.0f;

	virtual void BeginPlay() override;

	virtual void OnDeath() override;

	/*void OnHealthChanged(float Health);

private:
	void UpdateHealthWidgetVisibility();*/


};
