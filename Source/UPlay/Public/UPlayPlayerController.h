// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "UPlayCoreTypes.h"
#include "UPlayPlayerController.generated.h"


class UUPlayRespawnComponent;


UCLASS()
class UPLAY_API AUPlayPlayerController : public APlayerController
{
	GENERATED_BODY()
		
public:
	AUPlayPlayerController();

protected:
		UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Components")
		UUPlayRespawnComponent* RespawnComponent;
		virtual void BeginPlay() override;

		virtual void OnPossess(APawn* InPawn) override;

		virtual void SetupInputComponent() override;
private:
	void OnPauseGame();
	void OnMatchStateChanged(EUplayMatchState State);
	void OnMuteSound();
};
