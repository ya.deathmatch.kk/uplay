// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UplayGameDataWidget.generated.h"

/**
 * 
 */
class AUPlayGameModeBase;
class AUplayPlayerState;

UCLASS()
class UPLAY_API UUplayGameDataWidget : public UUserWidget
{
	GENERATED_BODY()

		/*UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetKillsNum() const;*/
		UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetCurrentRoundNum() const;
		UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetTotalRoundsNum() const;
		UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetRoundSecondsRemaining() const;
		UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetDeathsNum() const;

private:
	AUPlayGameModeBase* GetUplayGameMode() const;
	AUplayPlayerState* GetUplayPlayerState() const;




};
