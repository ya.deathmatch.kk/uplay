// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "UPlayEnvQueryTest_PickUpCanTake.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API UUPlayEnvQueryTest_PickUpCanTake : public UEnvQueryTest
{
	GENERATED_BODY()
public:

	UUPlayEnvQueryTest_PickUpCanTake(const FObjectInitializer& ObjectInitializer);
	
	
	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
		


};
