// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UPlayMenuGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API AUPlayMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

		AUPlayMenuGameModeBase();

	
};
