// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UPlayBaseWidget.generated.h"

/**
 * 
 */

class USoundCue;

UCLASS()
class UPLAY_API UUPlayBaseWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void Show();


protected:

	UPROPERTY(meta = (BindWidgetAnim), Transient)
		UWidgetAnimation* ShowAnimation;

		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* OpenSound;
};
