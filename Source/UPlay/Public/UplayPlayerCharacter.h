// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPlay/UplayCharacter.h"
#include "UplayPlayerCharacter.generated.h"

/**
 * 
 */






UCLASS()
class UPLAY_API AUplayPlayerCharacter : public AUplayCharacter
{
	GENERATED_BODY()
	
public:
	AUplayPlayerCharacter(const FObjectInitializer& ObjInit);

	virtual void OnDeath() override;



	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* SpringArmComponent;




	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveForward(float Value);

	void MoveRight(float Value);

	void BeginCrouch();
	void EndCrouch();

};
