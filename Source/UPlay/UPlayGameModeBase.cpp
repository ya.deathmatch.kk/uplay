// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayGameModeBase.h"
#include "UplayCharacter.h"
#include "UPlay/Public/UPlayPlayerController.h"
#include "UPlayHUD.h"
#include "AIController.h"
#include "UPlay/Public/UplayPlayerState.h"
#include "UPlay/Public/UPlayRespawnComponent.h"
#include "UPlay/Public/UPlayUtils.h"
#include "EngineUtils.h"
#include "UPlay/Public/UPlayGameInstance.h"
#include "UPlay/Public/UPlayAICharacter.h"
#include "UPlay/Public/UPlayCoreTypes.h"





DEFINE_LOG_CATEGORY_STATIC(LogUPlayGameModeBase, All, All);


constexpr static int32 MinRoundTimerForRespawn = 10;


AUPlayGameModeBase::AUPlayGameModeBase(): Super()
{
	DefaultPawnClass = AUplayCharacter:: StaticClass();
	PlayerControllerClass = AUPlayPlayerController::StaticClass();
	bReplicates = true;

	
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Game/Game/Blueprints/BP_UplayCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PlayerStateClass = AUplayPlayerState::StaticClass();

}

void AUPlayGameModeBase::StartPlay()
{
	Super::StartPlay();	

	SpawnBots();
	CreateTeamsInfo();
	CurrentRound = 1;
	StartRound();

	SetMatchState(EUplayMatchState::InProgress);
}

UClass* AUPlayGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{

	if (InController && InController->IsA<AAIController>())
	{
		return AIPawnClass;

	}


	return Super::GetDefaultPawnClassForController_Implementation(InController);
}





void AUPlayGameModeBase::SpawnBots()
{
	if (!GetWorld()) return;
	for (int32 i = 0; i < GameData.PlayersNum - 1; ++i)
	{

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const auto UPlayAIController = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnInfo);
		RestartPlayer(UPlayAIController);	

	}
}

void AUPlayGameModeBase::StartRound()
{
	RoundCountDown = GameData.RoundTime;
	GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &AUPlayGameModeBase::GameTimerUpdate, 1.0f, true);

}

void AUPlayGameModeBase::GameTimerUpdate()
{
	//UE_LOG(LogUPlayGameModeBase, Display, TEXT("Time: %i / Round: %i/%/"), RoundCountDown, CurrentRound, GameData.RoundsNum);



	if (--RoundCountDown == 0)
	{

		GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);

		if (CurrentRound + 1 <= GameData.RoundsNum)
		{
			++CurrentRound;
			ResetPlayers();
			StartRound();
		}

		else
		{
			GameOver();
		}

	}


}

void AUPlayGameModeBase::ResetPlayers()
{

	if (!GetWorld()) return;

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ResetOnePlayer(It->Get());
	}

}

void AUPlayGameModeBase::ResetOnePlayer(AController* Controller)
{
	if (Controller && Controller->GetPawn())
	{

		Controller->GetPawn()->Reset();

	}
	RestartPlayer(Controller);
	SetPlayerColor(Controller);
}

void AUPlayGameModeBase::CreateTeamsInfo()
{

	if (!GetWorld()) return;
	int32 TeamID = 1;
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{

		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<AUplayPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		PlayerState->SetTeamID(TeamID);
		PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
		PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : "Bot");
		SetPlayerColor(Controller);
		TeamID = TeamID == 1 ? 2 : 1;

	}
}

FLinearColor AUPlayGameModeBase::DetermineColorByTeamID(int32 TeamID) const
{


	if (TeamID - 1 < GameData.TeamColors.Num())
	{
		return GameData.TeamColors[TeamID - 1];
	}

	UE_LOG(LogUPlayGameModeBase, Warning, TEXT("No Color for team ID: %i, set to default: %s"), TeamID, *GameData.DefaultTeamColor.ToString());
	return GameData.DefaultTeamColor;
}

void AUPlayGameModeBase::SetPlayerColor(AController* Controller)
{

	if (!Controller) return;

	const auto Character = Cast<AUplayCharacter>(Controller->GetPawn());
	if (!Character) return;

	const auto PlayerState = Cast<AUplayPlayerState>(Controller->PlayerState);
	if (!PlayerState) return;

	Character->SetPlayerColor(PlayerState->GetTeamColor());
}



void AUPlayGameModeBase::Killed(AController* KillerController, AController* VictimController)
{
	const auto KillerPlayerState = KillerController ? Cast<AUplayPlayerState>(KillerController->PlayerState) : nullptr;
	const auto VictimPlayerState = VictimController ? Cast<AUplayPlayerState>(VictimController->PlayerState) : nullptr;

	if (KillerPlayerState)
	{

		KillerPlayerState->AddKill();

	}
	if (VictimPlayerState)
	{
		VictimPlayerState->AddDeath();
	}

	StartRespawn(VictimController);
}


void AUPlayGameModeBase::LogPlayerInfo()
{
	if (!GetWorld()) return;
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<AUplayPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		PlayerState->LogInfo();

	}




}





void AUPlayGameModeBase::StartRespawn(AController* Controller)
{
	const auto RespawnAvailable = RoundCountDown > MinRoundTimerForRespawn + GameData.RespawnTime;
	const auto RespawnComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayRespawnComponent>(Controller);
	if (!RespawnComponent) return;

	

	RespawnComponent->Respawn(GameData.RespawnTime);
	//ResetOnePlayer(Controller);
}

void AUPlayGameModeBase::RespawnRequest(AController* Controller)
{
	ResetOnePlayer(Controller);
}

void AUPlayGameModeBase::GameOver()
{
	//// GameOVer
	UE_LOG(LogUPlayGameModeBase, Display, TEXT("======Game Over====="))
		LogPlayerInfo();
	for (auto Pawn : TActorRange<APawn>(GetWorld())) // GetWorld() native Unreal function, cast to APawn and finding it in  World
	{

		if (Pawn)
		{

			Pawn->TurnOff();//Native Unreal Function 
			Pawn->DisableInput(nullptr); // Native unreal Function disables Input
			
		}

	}
	SetMatchState(EUplayMatchState::GameOver);
}

void AUPlayGameModeBase::SetMatchState(EUplayMatchState State)
{

	if (MatchState == State) return;

	MatchState = State;
	OnMatchStateChanged.Broadcast(MatchState);
}

bool AUPlayGameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{

	const auto PauseSet = Super::SetPause(PC, CanUnpauseDelegate);

	if (PauseSet)
	{
		SetMatchState(EUplayMatchState::Pause);
	}
	

	return PauseSet;
}

bool AUPlayGameModeBase::ClearPause()
{

	const auto PauseCleared = Super::ClearPause();
	if (PauseCleared)
	{
		SetMatchState(EUplayMatchState::InProgress);
	}
	return PauseCleared;
}
