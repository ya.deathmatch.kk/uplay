// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPlayPickableObject.h"
#include "UPlayHealthPickableObject.generated.h"

/**
 * 
 */
UCLASS()
class UPLAY_API AUPlayHealthPickableObject : public AUPlayPickableObject
{
	GENERATED_BODY()
	
	
protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp", meta = (ClampMin = "1.0", ClampMax = "10.0"))
	float HealthAmount = 10.0f;
		

public:
	virtual void OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor) override;

	
};
