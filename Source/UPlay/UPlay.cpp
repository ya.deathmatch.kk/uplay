// Copyright Epic Games, Inc. All Rights Reserved.

#include "UPlay.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UPlay, "UPlay" );
