// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayDamagableCharacter.h"
#include "UPlayHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TextRenderComponent.h"
#include "Engine/EngineTypes.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"



DEFINE_LOG_CATEGORY_STATIC(DamagableCharacterLog, All, All)

// Sets default values
AUPlsyDamagableCharacter::AUPlsyDamagableCharacter()
{



 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	bReplicates = true;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));






	HealthComponent = CreateDefaultSubobject<UUPlayHealthComponent>(TEXT("HealthComponent"));


	HealthTextComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("HealthTextComponent"));
	HealthTextComponent->SetupAttachment(GetRootComponent());




	TSubclassOf<UDamageType> DamageType;
}

// Called when the game starts or when spawned
void AUPlsyDamagableCharacter::BeginPlay()
{

	Super::BeginPlay();

	check(HealthComponent);
	check(HealthTextComponent);

	//OnTakeAnyDamage.AddDynamic(this, &AUPlsyDamagableCharacter::OnTakeAnyDamageHandle);
}

// Called every frame


// Called to bind functionality to input
void AUPlsyDamagableCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AUPlsyDamagableCharacter::GetDamage()
{
		
	if (HealthComponent->GetHealth() <= 0)
	{
		this->Destroy();


	}
	else
	{
		if (HealthComponent->Health < 100)
		{

			HealthComponent->Health += 0.01f;
		}
	}
}

//void AUPlsyDamagableCharacter::OnHealthChanged(UUPlayHealthComponent* HealthComp, float Health, float DamageAmount, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
//{
//	if (Health <= 0.0f)
//	{
//		this->Destroy();
//	}
//	UE_LOG(LogTemp, Warning, TEXT("The Player's Current Health is: %f"), Health);
//
//
//}
//
//void AUPlsyDamagableCharacter::DamagePlayerCharacter()
//{
//	UGameplayStatics::ApplyDamage(this, 20.0f, GetInstigatorController(), this, GenericDamageType);
//}
//
//void AUPlsyDamagableCharacter::OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
//{
//
//	UE_LOG(DamagableCharacterLog, Display, TEXT("Damage: %f"), Damage);
//}



void AUPlsyDamagableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	const auto Health = HealthComponent->GetHealth();
	
	
	
	HealthTextComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));
	GetDamage();
	
}