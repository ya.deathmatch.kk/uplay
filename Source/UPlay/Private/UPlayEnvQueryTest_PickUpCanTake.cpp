// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayEnvQueryTest_PickUpCanTake.h"
#include "UPlay/UPlayPickableObject.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"

UUPlayEnvQueryTest_PickUpCanTake::UUPlayEnvQueryTest_PickUpCanTake(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	Cost = EEnvTestCost::Low;
	ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
	SetWorkOnFloatValues(false);


}

void UUPlayEnvQueryTest_PickUpCanTake::RunTest(FEnvQueryInstance& QueryInstance) const
{

	const auto DataOwner = QueryInstance.Owner.Get();
	BoolValue.BindData(DataOwner, QueryInstance.QueryID);
	bool WantsBeTakable = BoolValue.GetValue();

	for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		const auto ItemActor = GetItemActor(QueryInstance, It.GetIndex());
		const auto PickUpActor = Cast<AUPlayPickableObject>(ItemActor);
		if (!PickUpActor) continue;

		const auto CouldBeTaken = PickUpActor->CouldBeTaken();
		It.SetScore(TestPurpose, FilterType, CouldBeTaken, WantsBeTakable);

	}

}
