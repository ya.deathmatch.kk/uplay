// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "UPlayGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/HorizontalBox.h"
#include "UPlayLevelItemWidget.h"
#include "Sound/SoundCue.h"
#include "MultiplayerSessionsSubsystem.h"

DEFINE_LOG_CATEGORY_STATIC(LogUPlayMenuWidget, All, All);



void UUPlayMenuWidget::NativeOnInitialized()
{

	Super::NativeOnInitialized();

	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &UUPlayMenuWidget::OnStartGame);
	}

	if (QuitGameButton)
	{
		QuitGameButton->OnClicked.AddDynamic(this, &UUPlayMenuWidget::OnQuitGame);
	}
	if (CreateGameButton)
	{
		CreateGameButton->OnClicked.AddDynamic(this, &UUPlayMenuWidget::OnCreateServer);
	}
	if (JoinGameButton)
	{
		JoinGameButton->OnClicked.AddDynamic(this, &UUPlayMenuWidget::OnJoinServer);
	}
	InitLevelItems();

}




void UUPlayMenuWidget::InitLevelItems()
{
	const auto UPlayGameInstance = GetUplayGameInstance();
	if (!UPlayGameInstance) return;

	checkf(UPlayGameInstance->GetLevelsData().Num() != 0, TEXT("Levels Data must not be empty!"));

	if (!LevelItemBox) return;
	LevelItemBox->ClearChildren();

	for (auto LevelData : UPlayGameInstance->GetLevelsData())
	{
		const auto LevelItemWidget = CreateWidget<UUPlayLevelItemWidget>(GetWorld(), LEvelItemWidgetClass);
		if (!LevelItemWidget) continue;

		LevelItemWidget->SetLevelData(LevelData);
		LevelItemWidget -> OnLevelSelected.AddUObject(this, &UUPlayMenuWidget::OnLevelSelected);

		LevelItemBox->AddChild(LevelItemWidget);
		LevelItemWidgets.Add(LevelItemWidget);
	}

	if (UPlayGameInstance->GetStartupLevel().LevelName.IsNone())
	{
		OnLevelSelected(UPlayGameInstance->GetLevelsData()[0]);
	}
	else
	{
		OnLevelSelected(UPlayGameInstance->GetStartupLevel());
	}



}

void UUPlayMenuWidget::OnLevelSelected(const FLevelData& Data)
{
	const auto UPlayGameInstance = GetUplayGameInstance();
	if (!UPlayGameInstance) return;

	UPlayGameInstance->SetStartupLEvel(Data);


	for (auto LevelItemWidget : LevelItemWidgets)
	{
		if (LevelItemWidget)
		{
			const auto IsSelected = Data.LevelName == LevelItemWidget->GetLevelData().LevelName;
			LevelItemWidget->SetSelected(IsSelected);
		}
	}


}



void UUPlayMenuWidget::OnStartGame()
{
	PlayAnimation(HideAnimation);
	UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
	
}


void UUPlayMenuWidget::OnCreateServer()
{
	/*const auto MultiplayerSessionSubsystem = Cast<UMultiplayerSessionsSubsystem>(GetWorld());
	MultiplayerSessionSubsystem->CreateServer("TestServer");
	UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
	MultiplayerSessionSubsystem->PrintString("From C++");*/
	
	
}

void UUPlayMenuWidget::OnJoinServer()
{
	/*const auto MultiplayerSessionSubsystem = Cast<UMultiplayerSessionsSubsystem>(GetWorld());
	MultiplayerSessionSubsystem->FindServer("TestServer");
	UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
	MultiplayerSessionSubsystem->PrintString("From C++");*/
}

void UUPlayMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{

	if (Animation != HideAnimation) return;
	const auto UPlayGameInstance = GetUplayGameInstance();
	if (!UPlayGameInstance) return;

	/*const FName StartupLevelName = "TestLevel";*/
	UGameplayStatics::OpenLevel(this, UPlayGameInstance->GetStartupLevel().LevelName);
}

void UUPlayMenuWidget::OnQuitGame()
{

	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);



}



UUPlayGameInstance* UUPlayMenuWidget::GetUplayGameInstance() const
{
	if (!GetWorld()) return nullptr;

	return GetWorld()->GetGameInstance<UUPlayGameInstance>();

	/*const auto MultiplayerSessionSubsystem = Cast<UMultiplayerSessionsSubsystem>(GetWorld());
	MultiplayerSessionSubsystem->CreateServer("TestServer");*/
}
