// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayWeaponComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Character.h"
#include "UPlayBaseWeapon.h"
#include "UPlay/UplayCharacter.h"

// Sets default values for this component's properties
UUPlayWeaponComponent::UUPlayWeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	
	// ...
}


// Called when the game starts
void UUPlayWeaponComponent::BeginPlay()
{

	

	Super::BeginPlay();
	CurrentWeaponIndex = 0;
	SpawnWeapons();
	EquipWeapon(CurrentWeaponIndex);
	
}

void UUPlayWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{

	CurrentWeapon = nullptr;
	for (auto Weapon : Weapons)
	{
		Weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Weapon->Destroy();
	}

	Weapons.Empty();
	Super::EndPlay(EndPlayReason);

}




void UUPlayWeaponComponent::SpawnWeapons()
{


	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character || !GetWorld()) return;
	
	for (auto WeaponClass : WeaponClasses)
	{
		auto Weapon = GetWorld()->SpawnActor<AUPlayBaseWeapon>(WeaponClass); //cast to base weapon and spawn it if character is in the world

		if (!Weapon) continue;

		Weapon->SetOwner(Character);
		Weapons.Add(Weapon);
		
		
		AttachWeaponToSocket(Weapon, Character->GetMesh(), WeaponArmorySocketName);
		
	}	
	
}

void UUPlayWeaponComponent::AttachWeaponToSocket(AUPlayBaseWeapon* Weapon, USceneComponent* SceneComponent, FName& SocketName)
{

	if (!Weapon || !SceneComponent) return;

	

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	Weapon->AttachToComponent(SceneComponent, AttachmentRules, SocketName);
	
}

void UUPlayWeaponComponent::EquipWeapon(int32 WeaponIndex)
{

	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character) return;
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
		AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponArmorySocketName);
		
	}



	CurrentWeapon = Weapons[WeaponIndex];
	AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponEquipSocketName);

}





void UUPlayWeaponComponent::StartFire()
{
	if (!CurrentWeapon) return;
	CurrentWeapon->StartFire();
}

void UUPlayWeaponComponent::StopFire()
{
	if (!CurrentWeapon) return;
	CurrentWeapon->StopFire();
}

bool UUPlayWeaponComponent::GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const
{

	if (CurrentWeapon)
	{
		AmmoData = CurrentWeapon->GetAmmoData();
		return true;
	}




	return false;
}

//void UUPlayWeaponComponent::Fire()
//{
//
//	if (!CurrentWeapon) return;
//	CurrentWeapon->Fire();
//}
void UUPlayWeaponComponent::NextWeapon()
{
	CurrentWeaponIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
	EquipWeapon(CurrentWeaponIndex);
}

bool UUPlayWeaponComponent::TryToAddAmmo(TSubclassOf<AUPlayBaseWeapon> WeaponType, int32 ClipsAmount)
{
	for (const auto Weapon: Weapons)
	{
		if (Weapon && Weapon->IsA(WeaponType))
		{
			return Weapon->TryToAddAmmo(ClipsAmount);
		}

	}

	return false;

}

bool UUPlayWeaponComponent::NeedAmmo(TSubclassOf<AUPlayBaseWeapon> WeaponType)
{
	for (const auto Weapon : Weapons)
	{
		if (Weapon && Weapon->IsA(WeaponType))
		{
			return !Weapon->IsAmmoFull();
		}

	}

	return false;
}

void UUPlayWeaponComponent::OnClipEmpty(AUPlayBaseWeapon* AmmoEmptyWeapon)
{

}



