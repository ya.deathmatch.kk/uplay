// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayFireService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "UPlayAIPerceptionComponent.h"
#include "UPlay/Public/UPlayUtils.h"
#include "UPlayWeaponComponent.h"



UUPlayFireService::UUPlayFireService()
{
	NodeName = "Fire";


}



void UUPlayFireService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();
	const auto Blackboard = OwnerComp.GetBlackboardComponent();

	const auto HasAim = Blackboard && Blackboard->GetValueAsObject(EnemyActorKey.SelectedKeyName);
	if (Controller)
	{

		const auto WeaponComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayWeaponComponent>(Controller->GetPawn());
		if (WeaponComponent)
		{
			HasAim ? WeaponComponent->StartFire() : WeaponComponent->StopFire();
		}



	}


	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);


}
