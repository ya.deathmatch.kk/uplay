// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayBaseWidget.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

void UUPlayBaseWidget::Show()
{
	PlayAnimation(ShowAnimation);

	UGameplayStatics::PlaySound2D(GetWorld(), OpenSound);
}
