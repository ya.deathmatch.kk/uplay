// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayPlayerHUDWidget.h"
#include "UPlay/UPlayHealthComponent.h"
#include "UPlayWeaponComponent.h"
#include "UPlay/Public/UPlayUtils.h"
#include "UPlay/Public/UplayPlayerState.h"
#include "Components/ProgressBar.h"




void UUPlayPlayerHUDWidget::NativeOnInitialized()
{

	Super::NativeOnInitialized();

	if (GetOwningPlayer())
	{
		GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UUPlayPlayerHUDWidget::OnNewPawn);
		OnNewPawn(GetOwningPlayerPawn());
	}

}





bool UUPlayPlayerHUDWidget::GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const
{
	
	const auto WeaponComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayWeaponComponent>(GetOwningPlayerPawn());
	if (!WeaponComponent) return false;

	return WeaponComponent->GetCurrentWeaponAmmoData(AmmoData);
}






void UUPlayPlayerHUDWidget::OnNewPawn(APawn* NewPawn)
{

	const auto HealthComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayHealthComponent>(NewPawn	);

	if (HealthComponent /*&& !HealthComponent->OnHealthChanged.IsBoundToObject(this)*/)
	{
		HealthComponent->OnHealthChanged.AddUObject(this, &UUPlayPlayerHUDWidget::OnHealthChanged);
		

	}
	UpdateHealthBar();
	
}

void UUPlayPlayerHUDWidget::OnHealthChanged(float Health, float HealthDelta)
{
	if (HealthDelta < 0.0f)
	{
		OnTakeDamage();
		
		if (!IsAnimationPlaying(DamageAnimation))
		{
			PlayAnimation(DamageAnimation);
		}
	}
	
	UpdateHealthBar();
}

bool UUPlayPlayerHUDWidget::IsPlayerAlive() const
{
	const auto HealthComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayHealthComponent>(GetOwningPlayerPawn());
	return HealthComponent && !HealthComponent->IsDead();
	//return false;
}


float UUPlayPlayerHUDWidget::GetHealthPercent() const
{
	/*const auto Player = GetOwningPlayerPawn();*/
	/*if (!Player) return 0.0f;*/

	/*const auto Component = Player->GetComponentByClass(UUPlayHealthComponent::StaticClass());*/
	const auto HealthComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayHealthComponent>(GetOwningPlayerPawn());

	if (!HealthComponent) return 0.0f;
	

	return HealthComponent->GetHealthPercent();


}
bool UUPlayPlayerHUDWidget::IsPlayerSpectating() const
{
	const auto Controller = GetOwningPlayer();
	return Controller && Controller->GetStateName() == NAME_Spectating;
	//return false;
}

int32 UUPlayPlayerHUDWidget::GetKillsNum() const
{
	const auto Controller = GetOwningPlayer();

	const auto PlayerState = Cast<AUplayPlayerState>(Controller->PlayerState);
	return PlayerState ? PlayerState->GetKillsNum() : 0;
}



void UUPlayPlayerHUDWidget::UpdateHealthBar()
{
	
	if (HealthProgressBar)
	{
		
		PlayAnimation(DamageAnimation);
		HealthProgressBar->SetFillColorAndOpacity(GetHealthPercent() > PercentColorThreshold ? GoodColor : BadColor);
	}
}

FString UUPlayPlayerHUDWidget::FormatBullets(int32 BulletsNum) const
{


	const int32 MaxLen = 3;
	const TCHAR PrefixSymbol = '0';

	auto BulletStr = FString::FromInt(BulletsNum);
	const auto SymbolIsNumToAdd = MaxLen - BulletStr.Len();

	if (SymbolIsNumToAdd > 0)
	{
		BulletStr = FString::ChrN(SymbolIsNumToAdd, PrefixSymbol).Append(BulletStr);

	}

	return BulletStr;
}