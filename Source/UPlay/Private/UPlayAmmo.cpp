// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayAmmo.h"
#include "Kismet/GameplayStatics.h"
#include "UPlayWeaponComponent.h"
#include "UPlayBaseWeapon.h"



DEFINE_LOG_CATEGORY_STATIC(LogBasePickup, All, All);



// Sets default values
AUPlayAmmo::AUPlayAmmo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	CollisionComponent->InitSphereRadius(50.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	SetRootComponent(CollisionComponent);




}

// Called when the game starts or when spawned
void AUPlayAmmo::BeginPlay()
{
	Super::BeginPlay();

	check(CollisionComponent);

	OnActorBeginOverlap.AddDynamic(this, &AUPlayAmmo::OnOverlap);
	
}

// Called every frame
void AUPlayAmmo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AUPlayAmmo::OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor)
{
	if ((OtherActor != this) && (OtherActor != nullptr))
	{
		MyCharacter = Cast<AUplayCharacter>(OtherActor);

		PickUpWasTaken();
		UE_LOG(LogBasePickup, Display, TEXT("Ammo was taken"));
		const auto WeaponComponent = Cast<UUPlayWeaponComponent>(OtherActor);

		WeaponComponent->TryToAddAmmo(WeaponType, BulletsAmount);

		
	}


}

bool AUPlayAmmo::GivePickUpTo(APawn* PlayerPawn)
{
	const auto WeaponComponent =  Cast<UUPlayWeaponComponent>(PlayerPawn);

	return WeaponComponent->TryToAddAmmo(WeaponType, BulletsAmount);
	
}

void AUPlayAmmo::PickUpWasTaken()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(false, true);
	}

	FTimerHandle RespawnTimerHandle;
	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &AUPlayAmmo::Respawn, RespawnTime);

}

void AUPlayAmmo::Respawn()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(true, true);
	}
}

