// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayAmmoPickupDecorator.h"
#include "AIController.h"
#include "UPlay/Public/UPlayUtils.h"
#include "UPlay/Public/UPlayWeaponComponent.h"



UUPlayAmmoPickupDecorator::UUPlayAmmoPickupDecorator()
{
	NodeName = "Need Ammo";
}

bool UUPlayAmmoPickupDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) return false;

	const auto WeaponComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayWeaponComponent>(Controller->GetPawn());
	if (!WeaponComponent) return false;

	return WeaponComponent->NeedAmmo(WeaponType);
}
