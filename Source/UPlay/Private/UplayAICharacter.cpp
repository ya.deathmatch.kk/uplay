// Fill out your copyright notice in the Description page of Project Settings.


#include "UplayAICharacter.h"
#include "UPlay/Public/UPlayAIController.h"
#include "GameFramework/CharacterMovementComponent.h" 
#include "UPlayAIWeaponComponent.h"
#include "BrainComponent.h"
#include "Components/WidgetComponent.h"
#include  "UPlay//Public/UPlayHealthBarWidget.h"
#include "UPlay/UPlayHealthComponent.h"
#include "Components/TextRenderComponent.h"


AUplayAICharacter::AUplayAICharacter(const FObjectInitializer& ObjInit) /*: Super(ObjInit) */ : Super(ObjInit.SetDefaultSubobjectClass<UUPlayAIWeaponComponent>("WeaponComponent"))
{

	AutoPossessAI = EAutoPossessAI::Disabled;
	AIControllerClass = AUPlayAIController::StaticClass();
	bReplicates = true;
	bUseControllerRotationYaw = false;
	if (GetCharacterMovement())
	{
		GetCharacterMovement()->bUseControllerDesiredRotation = true;
		GetCharacterMovement()->RotationRate = FRotator(0.0f, 200.0f, 0.0f);
	}

	HealthWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("HealthWidgetComponent");
	HealthWidgetComponent->SetupAttachment(GetRootComponent());
	HealthWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	HealthWidgetComponent->SetDrawAtDesiredSize(true);

	HealthTextComponent = CreateDefaultSubobject<UTextRenderComponent>("HealthTextComponent");
	HealthTextComponent->SetupAttachment(GetRootComponent());
	

}



void AUplayAICharacter::BeginPlay()
{
	Super::BeginPlay();
	check(HealthWidgetComponent);


}

void AUplayAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UpdateHealthWidgetVisibility();
	
	const auto Health = HealthComponent->GetHealth();
	HealthTextComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));



}

void AUplayAICharacter::OnDeath()
{
	Super::OnDeath();
	const auto UPlayController = Cast<AAIController>(Controller);
	if (UPlayController && UPlayController->BrainComponent)
	{
		UPlayController->BrainComponent->Cleanup();
	}
}

//void AUplayAICharacter::OnHealthChanged(float Health) // doesn't work properly, should chnge on health changed
//{
//	Super::OnHealthChanged(Health);
//
//	
//	const auto HealthBarWidget = CreateWidget<UUPlayHealthBarWidget>(HealthWidgetComponent->GetUserWidgetObject());
//	if (!HealthBarWidget) return;
//
//	HealthBarWidget->SetHealthPercent(HealthComponent->GetHealthPercent());
//
//}
//
//void AUplayAICharacter::UpdateHealthWidgetVisibility()
//{
//	if (!GetWorld() || !GetWorld()->GetFirstPlayerController() || !GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator()) return;
//
//
//	const auto PlayerLocation = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
//	const auto Distance = FVector::Distance(PlayerLocation, GetActorLocation());
//	HealthWidgetComponent->SetVisibility(Distance < HealthVisibilityDistance, true);
//
//
//}

