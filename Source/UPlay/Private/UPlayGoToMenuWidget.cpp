// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayGoToMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "UPlayGameInstance.h"


DEFINE_LOG_CATEGORY_STATIC(LogUPlayGoToMenu, All, All);

void UUPlayGoToMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (GoToMenuButton)
	{
		GoToMenuButton->OnClicked.AddDynamic(this, &UUPlayGoToMenuWidget::OnGoToMenu);
	}

}

void UUPlayGoToMenuWidget::OnGoToMenu()
{

	if (!GetWorld()) return;

	const auto UPlayGameInstance = GetWorld()->GetGameInstance<UUPlayGameInstance>();
	if (!UPlayGameInstance) return;

	if (UPlayGameInstance->GetMenuLevelName().IsNone())
	{

		UE_LOG(LogUPlayGoToMenu, Display, TEXT("Menu Level Name Is NONE"));
		return;
	}

	/*const FName StartupLevelName = "TestLevel";*/
	UGameplayStatics::OpenLevel(this, UPlayGameInstance->GetMenuLevelName());


}
