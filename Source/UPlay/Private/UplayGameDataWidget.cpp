// Fill out your copyright notice in the Description page of Project Settings.


#include "UplayGameDataWidget.h"
#include "UPlay/UPlayGameModeBase.h"
#include "UPlay/Public/UplayPlayerState.h"

//int32 UUplayGameDataWidget::GetKillsNum() const
//{
//	const auto PlayerState = GetUplayPlayerState();
//	return PlayerState ? PlayerState->GetKillsNum() : 0;
//}

int32 UUplayGameDataWidget::GetCurrentRoundNum() const
{
	const auto GameMode = GetUplayGameMode();
	return GameMode ? GameMode->GetCurrentRoundsNum() : 0;
}

int32 UUplayGameDataWidget::GetTotalRoundsNum() const
{
	const auto GameMode = GetUplayGameMode();
	return GameMode ? GameMode->GetGameData().RoundsNum : 0;
}

int32 UUplayGameDataWidget::GetRoundSecondsRemaining() const
{
	const auto GameMode = GetUplayGameMode();
	return GameMode ? GameMode->GetRoundSecondsRemaining() : 0;
}

int32 UUplayGameDataWidget::GetDeathsNum() const
{
	const auto PlayerState = GetUplayPlayerState();
	return PlayerState ? PlayerState->GetDeathsNum() : 0;
}

AUPlayGameModeBase* UUplayGameDataWidget::GetUplayGameMode() const
{
	return GetWorld() ? Cast<AUPlayGameModeBase>(GetWorld()->GetAuthGameMode()) : nullptr;
}

AUplayPlayerState* UUplayGameDataWidget::GetUplayPlayerState() const
{
	return GetOwningPlayer() ? Cast<AUplayPlayerState>(GetOwningPlayer()->PlayerState) : nullptr;
}
