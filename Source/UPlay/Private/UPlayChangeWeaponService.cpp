// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayChangeWeaponService.h"
#include "AIController.h"
#include "UPlay/Public/UPlayUtils.h"
#include "UPlayWeaponComponent.h"

UUPlayChangeWeaponService::UUPlayChangeWeaponService()
{
	NodeName = "Change Weapon";
}

void UUPlayChangeWeaponService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{

	const auto Controller = OwnerComp.GetAIOwner();
	if (Controller)
	{

		const auto WeaponComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayWeaponComponent>(Controller->GetPawn());
		if (WeaponComponent && Probability >0 && FMath::FRand() <= Probability)
		{
			WeaponComponent->NextWeapon();
		}

	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);



}




