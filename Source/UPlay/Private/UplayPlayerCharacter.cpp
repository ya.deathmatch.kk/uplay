// Fill out your copyright notice in the Description page of Project Settings.


#include "UplayPlayerCharacter.h"
#include "UPlay/UplayCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "UPlay/UPlayHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"
#include "UPlayBaseWeapon.h"
#include "UPlayWeaponComponent.h"
#include "UPlay/UPlayProjectile.h"
#include "GameFramework/CharacterMovementComponent.h"

AUplayPlayerCharacter::AUplayPlayerCharacter(const FObjectInitializer& ObjInit) : Super(ObjInit)
{


	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCameraComponent"));
	FirstPersonCameraComponent->SetupAttachment(RootComponent);

	GetMesh()->SetOwnerNoSee(false);
	

	// ������������� ������� ������ �� ������ ����
	FirstPersonCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f,50.0f + BaseEyeHeight));
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	
}





void AUplayPlayerCharacter::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	FirstPersonCameraComponent->FieldOfView = FMath::Lerp<float>(90.0f, 60.0f, ZoomFactor);
	
}

void AUplayPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(WeaponComponent);
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AUplayPlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUplayPlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("CameraPitch", this, &AUplayPlayerCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("CameraYaw", this, &AUplayPlayerCharacter::AddControllerYawInput);

	//Hook up events for "ZoomIn"
	PlayerInputComponent->BindAction("ZoomIn", IE_Pressed, this, &AUplayPlayerCharacter::ZoomIn);
	PlayerInputComponent->BindAction("ZoomIn", IE_Released, this, &AUplayPlayerCharacter::ZoomOut);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AUplayPlayerCharacter::Jump);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AUplayPlayerCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AUplayPlayerCharacter::EndCrouch);
	
	//PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AUplayPlayerCharacter::Fire);
	//Bind OnDamagePressed Action Event
	
	PlayerInputComponent->BindAction("Fire", IE_Pressed, WeaponComponent, &UUPlayWeaponComponent::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, WeaponComponent, &UUPlayWeaponComponent::StopFire);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, WeaponComponent, &UUPlayWeaponComponent::NextWeapon);
}

void AUplayPlayerCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AUplayPlayerCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AUplayPlayerCharacter::BeginCrouch()
{
	Crouch();
}

void AUplayPlayerCharacter::EndCrouch()
{
	UnCrouch();
}




void AUplayPlayerCharacter::OnDeath()
{
	Super::OnDeath();
	if (Controller)
	{
		Controller->ChangeState(NAME_Spectating);
	}

}