// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayAIWeaponComponent.h"
#include "UPlayBaseWeapon.h"


void UUPlayAIWeaponComponent::StartFire()
{
	if (CurrentWeapon->IsAmmoEmpty())
	{
		NextWeapon();
	}
	else
	{
		CurrentWeapon->StartFire();
		if (CurrentWeapon->IsAmmoEmpty())
		{
			NextWeapon();
		}
	}
}

//void UUPlayAIWeaponComponent::StopFire()
//{
//}

void UUPlayAIWeaponComponent::NextWeapon()
{

	int32 NextIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
	while (NextIndex != CurrentWeaponIndex)
	{
		if (!Weapons[NextIndex]->IsAmmoEmpty()) break;
		NextIndex = (NextIndex + 1) % Weapons.Num();
	}

	if (CurrentWeaponIndex != NextIndex)
	{
		CurrentWeaponIndex = NextIndex;
		EquipWeapon(CurrentWeaponIndex);
	}


}
