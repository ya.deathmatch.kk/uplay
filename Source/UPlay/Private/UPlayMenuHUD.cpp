// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayMenuHUD.h"
#include "UPlayBaseWidget.h"

void AUPlayMenuHUD::BeginPlay()
{

	Super::BeginPlay();
	bReplicates = true;
	if (MenuWidgetClass)
	{
		const auto MenuWidget = CreateWidget<UUPlayBaseWidget>(GetWorld(), MenuWidgetClass);
		if (MenuWidget)
		{
			MenuWidget->AddToViewport();
			MenuWidget->Show();
		}


	}

}
