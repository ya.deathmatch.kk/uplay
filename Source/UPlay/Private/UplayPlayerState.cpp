// Fill out your copyright notice in the Description page of Project Settings.


#include "UplayPlayerState.h"

DEFINE_LOG_CATEGORY_STATIC(LogUplayPlayerState, All, All);

void AUplayPlayerState::LogInfo()
{

	UE_LOG(LogUplayPlayerState, Display, TEXT("TeamID: %i, Kills: %i, Deaths: %i"), TeamID, KillsNum, DeathsNum);
}
