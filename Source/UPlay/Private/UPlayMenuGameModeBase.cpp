// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayMenuGameModeBase.h"
#include "UPlay/Public/UPlayMenuHUD.h"
#include "UPlay/Public/UPlayMenuPlayerController.h"




AUPlayMenuGameModeBase::AUPlayMenuGameModeBase()
{
	PlayerControllerClass = AUPlayMenuPlayerController::StaticClass();
	HUDClass = AUPlayMenuHUD::StaticClass();
	bReplicates = true;
}
