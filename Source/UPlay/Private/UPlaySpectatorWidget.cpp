// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlaySpectatorWidget.h"
#include "UPlay//Public/UPlayUtils.h"
#include "UPlay//Public/UPlayRespawnComponent.h"

bool UUPlaySpectatorWidget::GetRespawnTime(int32& CountDownTime) const
{
	const auto RespawnComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayRespawnComponent>(GetOwningPlayer());
	if (!RespawnComponent || !RespawnComponent->IsRespawnInProgress()) return false;

	CountDownTime = RespawnComponent->GetRespawnCountDown();
	return true;

}
