// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayBaseWeapon.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFrameWork/Character.h"
#include "GameFrameWork/Controller.h"
#include "Engine/DamageEvents.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Particles/ParticleSystem.h"




DEFINE_LOG_CATEGORY_STATIC(LogBaseWeapon, All, All);


// Sets default values
AUPlayBaseWeapon::AUPlayBaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh");
	SetRootComponent(WeaponMesh);



}

// Called when the game starts or when spawned
void AUPlayBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	CurrentAmmo = DefaultAmmo;
}

void AUPlayBaseWeapon::DecreaseAmmo()
{
	CurrentAmmo.Bullets--;
	if (CurrentAmmo.Bullets <= 0)
	{
		StopFire();	
	}
	

	if (IsClipEmpty() && !IsAmmoEmpty())
	{
		ChangeClip();
	}
}

bool AUPlayBaseWeapon::IsAmmoEmpty() const
{
	return !CurrentAmmo.Infinite && CurrentAmmo.Clips ==0 && IsClipEmpty();
}

bool AUPlayBaseWeapon::IsClipEmpty() const
{
	return CurrentAmmo.Bullets == 0;
}

void AUPlayBaseWeapon::ChangeClip()
{
	if (!CurrentAmmo.Infinite)
	{
		if (CurrentAmmo.Clips == 0)
		{
			/*UE_LOG(LogBaseWeapon, Warning, TEXT("No more clips"));*/
			return;
		}
		CurrentAmmo.Clips--;
	}
	CurrentAmmo.Bullets = DefaultAmmo.Bullets;

	/*UE_LOG(LogBaseWeapon, Display, TEXT("------Change Clip---"));*/
}

void AUPlayBaseWeapon::LogAmmo()
{

	FString AmmoInfo = "Ammo: " + FString::FromInt(CurrentAmmo.Bullets) + "/";
	AmmoInfo += CurrentAmmo.Infinite ? "Infinite" : FString::FromInt(CurrentAmmo.Clips);
	UE_LOG(LogBaseWeapon, Display, TEXT("%s"), *AmmoInfo);
}

bool AUPlayBaseWeapon::IsAmmoFull() const
{
	return CurrentAmmo.Clips == DefaultAmmo.Clips &&  //
		CurrentAmmo.Bullets == DefaultAmmo.Bullets;
}

bool AUPlayBaseWeapon::TryToAddAmmo(int32 ClipsAmount)
{
	
	if (CurrentAmmo.Infinite || IsAmmoFull() || ClipsAmount <=0) return false;

	CurrentAmmo.Bullets = FMath::Clamp(11 + CurrentAmmo.Bullets, 0, 30);
	



	//if (IsAmmoEmpty())
	//{
	//	UE_LOG(LogBaseWeapon, Display, TEXT("AmmoWasEmpty"));
	//	/*CurrentAmmo.Clips = FMath::Clamp(ClipsAmount, 0, DefaultAmmo.Clips + 1);*/
	//	
	//	
	//}
	//else if (CurrentAmmo.Clips < DefaultAmmo.Clips)
	//{

	//	const auto NextClipsAmount = CurrentAmmo.Clips + ClipsAmount;
	//	if(DefaultAmmo.Clips - NextClipsAmount >= 0)
	//	{
	//		DefaultAmmo.Clips = NextClipsAmount;
	//		UE_LOG(LogBaseWeapon, Display, TEXT("Clips were added"));
	//		
	//	}
	//	else
	//	{
	//		CurrentAmmo.Clips = DefaultAmmo.Clips;
	//		CurrentAmmo.Bullets = DefaultAmmo.Bullets;
	//		UE_LOG(LogBaseWeapon, Display, TEXT("Ammo is Full now"));
	//		
	//	}
	//	

	//}

	//else
	//{
	//	/*CurrentAmmo.Bullets = DefaultAmmo.Bullets;*/
	//	CurrentAmmo.Bullets = FMath::Clamp(8, 0, DefaultAmmo.Bullets + 1);
	//	UE_LOG(LogBaseWeapon, Display, TEXT("Bullets were added"));
	//	
	//}


	return true;
}

void AUPlayBaseWeapon::MakeShot()  {}

void AUPlayBaseWeapon::StartFire() {}

void AUPlayBaseWeapon::StopFire() {}





//APlayerController* AUPlayBaseWeapon::GetPlayerController() const
//{
//	const auto Player = Cast<ACharacter>(GetOwner());
//	/*if (!Player) return;*/
//
//	return Player->GetController<APlayerController>();
//}

bool AUPlayBaseWeapon::GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const
{

	const auto AUplayCharacter = Cast<ACharacter>(GetOwner());
	if (!AUplayCharacter) return false;
	if (AUplayCharacter->IsPlayerControlled())
	{
	
		const auto Controller = AUplayCharacter->GetController<APlayerController>();
		if (!Controller) return false;
		Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);
	
	}
	else
	{
		ViewLocation = GetMuzzleWorldLocation();
		ViewRotation = WeaponMesh->GetSocketRotation(MuzzleSocketName);
	}



	return true;

}

FVector AUPlayBaseWeapon::GetMuzzleWorldLocation() const
{

	return WeaponMesh->GetSocketLocation(MuzzleSocketName);

}

FRotator AUPlayBaseWeapon::GetMuzzleWorldRotation() const
{
	return WeaponMesh->GetSocketRotation(MuzzleSocketName);
}

bool AUPlayBaseWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{

	
	FVector ViewLocation;
	FRotator ViewRotation;
	if (!GetPlayerViewPoint(ViewLocation, ViewRotation)) return false;

	
	TraceStart = ViewLocation;
	const FVector ShootDirection = ViewRotation.Vector();
	TraceEnd = TraceStart + ShootDirection * TraceMaxDistance;

	return true;
	
}

void AUPlayBaseWeapon::MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd)
{

	if (!GetWorld()) return;
	FCollisionQueryParams CollsionParams;
	CollsionParams.bReturnPhysicalMaterial = true;
	CollsionParams.AddIgnoredActor(GetOwner());




	
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollsionParams);
	


}

UNiagaraComponent* AUPlayBaseWeapon::SpawnMuzzleFX()
{

	return UNiagaraFunctionLibrary::SpawnSystemAttached(MuzzleFX,
		WeaponMesh,
		MuzzleSocketName,
		FVector::ZeroVector,
		FRotator::ZeroRotator,
		EAttachLocation::SnapToTarget, true);

		
	
}





