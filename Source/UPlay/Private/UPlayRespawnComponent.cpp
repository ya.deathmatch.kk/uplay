// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayRespawnComponent.h"
#include "UPlay/UPlayGameModeBase.h"


// Sets default values for this component's properties
UUPlayRespawnComponent::UUPlayRespawnComponent()
{
	
	PrimaryComponentTick.bCanEverTick = false;
	
	
}

void UUPlayRespawnComponent::Respawn(int32 RespawnTime)
{

	if (!GetWorld()) return;
	RespawnCountDown = RespawnTime;
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &UUPlayRespawnComponent::RespawnTimerUpdate, 1.0f, true);


}



void UUPlayRespawnComponent::RespawnTimerUpdate()
{
	if (--RespawnCountDown == 0)
	{
		if (!GetWorld()) return;
		GetWorld()->GetTimerManager().ClearTimer(RespawnTimerHandle);

		const auto GameMode = Cast<AUPlayGameModeBase>(GetWorld()->GetAuthGameMode());
		if (!GameMode) return;

		GameMode->RespawnRequest(Cast<AController>(GetOwner()));


	}

}


bool UUPlayRespawnComponent::IsRespawnInProgress() const
{
	return GetWorld() && GetWorld() -> GetTimerManager().IsTimerActive(RespawnTimerHandle);
}