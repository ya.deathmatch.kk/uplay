// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayPauseWidget.h"
#include "GameFramework/GameModeBase.h"
#include "Components/Button.h"


void UUPlayPauseWidget::NativeOnInitialized()
{

	Super::NativeOnInitialized();

	if (ClearPauseButton)
	{
		ClearPauseButton->OnClicked.AddDynamic(this, &UUPlayPauseWidget::OnClearPause);
	}

	
}

void UUPlayPauseWidget::OnClearPause()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode()->ClearPause()) return;

	GetWorld()->GetAuthGameMode()->ClearPause();
}
