// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayRifleWeapon.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Engine/DamageEvents.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/AudioComponent.h"
#include "UPlayWeaponFXComponent.h"

AUPlayRifleWeapon::AUPlayRifleWeapon()
{

	bReplicates = true;
	WeaponFXComponent = CreateDefaultSubobject<UUPlayWeaponFXComponent>("WeaponFXComponent");
	WeaponFXComponent->SetIsReplicated(true);

}


void AUPlayRifleWeapon::BeginPlay()
{
	Super::BeginPlay();
	check(WeaponFXComponent);

}
void AUPlayRifleWeapon::StartFire()
{

	if (CurrentAmmo.Bullets > 0)
	{
		
		MakeShot();
		GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &AUPlayBaseWeapon::MakeShot, TimeBetweenShots, true);
	}
	


}

void AUPlayRifleWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(ShotTimerHandle);
	SetFXActive(false);
}



void AUPlayRifleWeapon::MakeShot()
{
	if (!GetWorld() || IsAmmoEmpty())
	{
		StopFire();
		return;
	}
	FVector TraceStart, TraceEnd;
	if (!GetTraceData(TraceStart, TraceEnd)) 
	{
		StopFire();
		return;
	}
		
		


	FHitResult HitResult;

	MakeHit(HitResult, TraceStart, TraceEnd);
	FVector TraceFXEnd = TraceEnd;

	if (HitResult.bBlockingHit)
	{
		TraceFXEnd = HitResult.ImpactPoint;
		MakeDamage(HitResult);
		WeaponFXComponent->PlayImpactFX(HitResult);

	}

	SpawnTraceFX(GetMuzzleWorldLocation(), TraceFXEnd);
	DecreaseAmmo();
	UGameplayStatics::SpawnEmitterAtLocation(this, FireFlash, GetMuzzleWorldLocation(), FRotator::ZeroRotator, true, EPSCPoolMethod::AutoRelease);

	InitFX();
	
}


/*DrawDebugLine(GetWorld(), GetMuzzleWorldLocation(), HitResult.ImpactPoint, FColor::Red, false, 3.0f, 0, 3.0f);
		DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 10.0f, 24, FColor::Red, false, 5.0f);*/

bool AUPlayRifleWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{


	FVector ViewLocation;
	FRotator ViewRotation;
	if (!GetPlayerViewPoint(ViewLocation, ViewRotation)) return false;
	
	const auto HalfRad = FMath::DegreesToRadians(BulletSpread);
	TraceStart = ViewLocation;
	const FVector ShootDirection = FMath::VRandCone(ViewRotation.Vector(), HalfRad);
	TraceEnd = TraceStart + ShootDirection * TraceMaxDistance;

	return true;

}

void AUPlayRifleWeapon::MakeDamage(const FHitResult& HitResult)
{


	const auto DamagedActor = HitResult.GetActor();
	if (!DamagedActor) return;

	DamagedActor->TakeDamage(DamageAmount, FDamageEvent(), GetController(), this);
}

AController* AUPlayRifleWeapon::GetController() const
{
	const auto Pawn = Cast<APawn>(GetOwner());
	return Pawn ? Pawn->GetController() : nullptr;
}

void AUPlayRifleWeapon::InitFX()
{
	
	
	MuzzleFXComponent = SpawnMuzzleFX();
	FireAudioComponent = UGameplayStatics::SpawnSoundAttached(FireSound,
		WeaponMesh,
		MuzzleSocketName);

	
	SetFXActive(true);
}

void AUPlayRifleWeapon::SetFXActive(bool IsActive)
{
	if (MuzzleFXComponent)
	{
		MuzzleFXComponent->SetPaused(!IsActive);
		MuzzleFXComponent->SetVisibility(IsActive, true);

	}
	if (FireAudioComponent)
	{
		IsActive ? FireAudioComponent->Play() : FireAudioComponent->Stop();
	}
}

void AUPlayRifleWeapon::SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd)
{
	const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, TraceStart);
	if (TraceFXComponent)
	{
		TraceFXComponent->SetNiagaraVariableVec3(TraceTargetName, TraceEnd);
	}

}
