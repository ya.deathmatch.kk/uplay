// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayHealthPercentDecorator.h"
#include "AIController.h"
#include "UPlay/Public/UPlayUtils.h"
#include "UPlay/UPlayHealthComponent.h"

UUPlayHealthPercentDecorator::UUPlayHealthPercentDecorator()
{

	NodeName = "Health Percent";
}

bool UUPlayHealthPercentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) return false;

	const auto HealthComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayHealthComponent>(Controller->GetPawn());
	if (!HealthComponent) return false;

	return HealthComponent->GetHealthPercent() <= HealthPercent;
}
