// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayAIController.h"
#include "UplayAICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "UPlay/Public/UPlayAIPerceptionComponent.h"
#include "UPlay/Public/UPlayRespawnComponent.h"







AUPlayAIController::AUPlayAIController()
{

	UPlayAIPerceptionComponent = CreateDefaultSubobject<UUPlayAIPerceptionComponent>("PerceptionComponent");
	SetPerceptionComponent(*UPlayAIPerceptionComponent);
	RespawnComponent = CreateDefaultSubobject<UUPlayRespawnComponent>("RespawnComponent");
	bReplicates = true;
	bWantsPlayerState = true; // Adds Player State for AI

}

void AUPlayAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	const auto UPlayCharacter = Cast<AUplayAICharacter>(InPawn);

	if (UPlayCharacter)
	{
		RunBehaviorTree(UPlayCharacter->BehaviorTreeAsset);
	}

}

void AUPlayAIController::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	const auto AimActor = GetFocusOnActor();
	SetFocus(AimActor);
}

AActor* AUPlayAIController::GetFocusOnActor() const
{

	if (!GetBlackboardComponent()) return nullptr;


	return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
