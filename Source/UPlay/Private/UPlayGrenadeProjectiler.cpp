// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayGrenadeProjectiler.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UPlayWeaponFXComponent.h"
#include "Particles/ParticleSystem.h"


// Sets default values
AUPlayGrenadeProjectiler::AUPlayGrenadeProjectiler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
    bReplicates = true;

	// Use a sphere as a simple collision representation.
	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	// Set the sphere's collision radius.
	CollisionComponent->InitSphereRadius(15.0f);

    CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
    CollisionComponent->bReturnMaterialOnMove = true;
	SetRootComponent(CollisionComponent);

	MovementComponent = CreateDefaultSubobject< UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
    MovementComponent->InitialSpeed = 2000.0f;
    MovementComponent->ProjectileGravityScale = 0.0f;

    WeaponFXComponent = CreateDefaultSubobject<UUPlayWeaponFXComponent>("WeaponFXComponent");

}



// Called when the game starts or when spawned
void AUPlayGrenadeProjectiler::BeginPlay()
{
	Super::BeginPlay();

	check(MovementComponent);
    check(CollisionComponent);
    check(WeaponFXComponent);


    CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true);
    CollisionComponent->OnComponentHit.AddDynamic(this, &AUPlayGrenadeProjectiler::OnProjectileHit);
    SetLifeSpan(LifeSeconds);
    
}




// Called every frame
void AUPlayGrenadeProjectiler::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    MovementComponent->Velocity = ShotDirection * MovementComponent->InitialSpeed;
    
}


void AUPlayGrenadeProjectiler::SetShotDirection(const FVector& Direction)
{
    ShotDirection = Direction;


}

void AUPlayGrenadeProjectiler::OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    if (!GetWorld()) return;

    MovementComponent->StopMovementImmediately();

    // make Damage

    UGameplayStatics::ApplyRadialDamage(GetWorld(),//
        DamageAmount,//
        GetActorLocation(), //
        DamageRadius, //
        UDamageType::StaticClass(),//
        { GetOwner() },//
        this,//
        GetController(),//
        DoFullDamage);


    //DrawDebugSphere(GetWorld(), GetActorLocation(), DamageRadius, 24, FColor::Red, false, 5.0f);
    WeaponFXComponent->PlayImpactFX(Hit);
    FVector SpawnLocation = GetActorLocation();
    UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionEffect, SpawnLocation, FRotator::ZeroRotator, true, EPSCPoolMethod::AutoRelease);
    //
    Destroy();
}

AController* AUPlayGrenadeProjectiler::GetController() const
{
    const auto Pawn = Cast<APawn>(GetOwner());
    return Pawn ? Pawn->GetController() : nullptr;
}