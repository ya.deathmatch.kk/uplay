// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayGameOverWidgets.h"
#include "UplayPlayerState.h"
#include "Components/VerticalBox.h"
#include "UPlayStatRowWidget.h"
#include "UPlay/UPlayGameModeBase.h"
#include "UPlayUtils.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"


//bool UUPlayGameOverWidgets::Initialize()
//{
//
//    if (GetWorld())
//    {
//        const auto GameMode = Cast<AUPlayGameModeBase>(GetWorld()->GetAuthGameMode());
//        if (GameMode)
//        {
//            GameMode->OnMatchStateChanged.AddUObject(this, &UUPlayGameOverWidgets::OnMatchStateChange);
//        }
//    }
//	return Super::Initialize();
//}

void UUPlayGameOverWidgets::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GetWorld())
    {
        const auto GameMode = Cast<AUPlayGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &UUPlayGameOverWidgets::OnMatchStateChange);
        }
    }

    if (ResetLevelButton)
    {
        ResetLevelButton->OnClicked.AddDynamic(this, &UUPlayGameOverWidgets::OnResetLevel);
    }


}

void UUPlayGameOverWidgets::OnMatchStateChange(EUplayMatchState State)
{
    if (State == EUplayMatchState::GameOver)
    {
        UpdatePlayerStat();
    }
}

void UUPlayGameOverWidgets::UpdatePlayerStat()
{

    if (!GetWorld() || !PlayerStatBox) return;

    PlayerStatBox->ClearChildren();

    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {

        const auto Controller = It->Get();
        if (!Controller) continue;

        const auto PlayerState = Cast<AUplayPlayerState>(Controller->PlayerState);
        if (!PlayerState) continue;

        const auto PlayerStatRowWidget = CreateWidget<UUPlayStatRowWidget>(GetWorld(), PlayerStatRowWidgetClass);
        if (!PlayerStatRowWidget) continue;


        PlayerStatRowWidget->SetPlayerName(FText::FromString(PlayerState->GetPlayerName()));
        PlayerStatRowWidget->SetKills(UPlayUtils::FromTextFromInt(PlayerState->GetKillsNum()));
        PlayerStatRowWidget->SetDeaths(UPlayUtils::FromTextFromInt(PlayerState->GetDeathsNum()));
        PlayerStatRowWidget->SetTeam(UPlayUtils::FromTextFromInt(PlayerState->GetTeamID()));
        PlayerStatRowWidget->SetPlayerIndicatorVisibility(Controller->IsPlayerController());
        PlayerStatRowWidget->SetTeamColor(PlayerState->GetTeamColor());

        PlayerStatBox->AddChild(PlayerStatRowWidget);


    }

}

void UUPlayGameOverWidgets::OnResetLevel()
{
    const FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(this);
    UGameplayStatics::OpenLevel(this, FName(CurrentLevelName));



}
