// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayPlayerController.h"
#include "UPlay/Public/UPlayRespawnComponent.h"
#include "UPlay/UPlayGameModeBase.h"
#include "UPlayGameInstance.h"

AUPlayPlayerController::AUPlayPlayerController()
{
	bReplicates = true;
	RespawnComponent = CreateDefaultSubobject<UUPlayRespawnComponent> ("Respawn Component");
}

void AUPlayPlayerController::BeginPlay()
{

	Super::BeginPlay();

	if (GetWorld())
	{
		const auto GameMode = Cast<AUPlayGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnMatchStateChanged.AddUObject(this, &AUPlayPlayerController::OnMatchStateChanged);
		}
	}
}

void AUPlayPlayerController::OnMatchStateChanged(EUplayMatchState State)
{
	if (State == EUplayMatchState::InProgress)
	{

		SetInputMode(FInputModeGameOnly());
		bShowMouseCursor = false;
	}
	else
	{
		SetInputMode(FInputModeUIOnly());
		bShowMouseCursor = true;
	}

}




void AUPlayPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	OnNewPawn.Broadcast(InPawn);
}

void AUPlayPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (!InputComponent) return;

	InputComponent->BindAction("Pause", IE_Pressed, this,  &AUPlayPlayerController::OnPauseGame);
	InputComponent->BindAction("Mute", IE_Pressed, this, &AUPlayPlayerController::OnMuteSound);
}

void AUPlayPlayerController::OnPauseGame()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode()) return;


	GetWorld()->GetAuthGameMode()->SetPause(this);

}


void AUPlayPlayerController::OnMuteSound()
{
	if (!GetWorld()) return;


	const auto UPlayGameInstance = GetWorld()->GetGameInstance<UUPlayGameInstance>();
	if (!UPlayGameInstance) return;

	UPlayGameInstance->ToggleVolume();
}