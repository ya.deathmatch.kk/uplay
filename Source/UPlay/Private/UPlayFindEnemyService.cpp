// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayFindEnemyService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "UPlayAIPerceptionComponent.h"
#include "UPlay/Public/UPlayUtils.h"




UUPlayFindEnemyService::UUPlayFindEnemyService()
{
	NodeName = "Find Enemy";
}

void UUPlayFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{

	
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if (Blackboard)
	{
		const auto Controller = OwnerComp.GetAIOwner();
		const auto PerceptionComponent = UPlayUtils::GetUPlayPlayerComponent<UUPlayAIPerceptionComponent>(Controller);
		if (PerceptionComponent)
		{
			Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComponent->GetClosestEnemy());
		}
	}
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
