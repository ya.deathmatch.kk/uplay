// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayLevelItemWidget.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "UPlayGameInstance.h"



void UUPlayLevelItemWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();


	if (LevelSelectedButton)
	{
		LevelSelectedButton->OnClicked.AddDynamic(this, &UUPlayLevelItemWidget::OnLevelItemClicked);
		LevelSelectedButton->OnHovered.AddDynamic(this, &UUPlayLevelItemWidget::OnLevelItemHovered);
		LevelSelectedButton->OnUnhovered.AddDynamic(this, &UUPlayLevelItemWidget::OnLevelItemUnhovered);
	}
}



void UUPlayLevelItemWidget::OnLevelItemClicked()
{
	OnLevelSelected.Broadcast(LevelData);
}



void UUPlayLevelItemWidget::SetLevelData(const FLevelData& Data)
{

	LevelData = Data;

	if (LevelNameTextBlock)
	{
		LevelNameTextBlock->SetText(FText::FromName(Data.LevelDisplayName));
	}

	if (LevelImage)
	{
		LevelImage->SetBrushFromTexture(Data.LevelThumb);
	}
	
}



void UUPlayLevelItemWidget::SetSelected(bool IsSelected)
{


	if (LevelImage)
	{
		LevelImage->SetColorAndOpacity(IsSelected ? FLinearColor::Red : FLinearColor::White);
	}
	if (FrameImage)
	{
		FrameImage->SetVisibility(IsSelected ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
	}
}


void UUPlayLevelItemWidget::OnLevelItemHovered()
{
	/*if (FrameImage)
	{
		FrameImage->SetVisibility( ESlateVisibility::Hidden);
	}*/
}

void UUPlayLevelItemWidget::OnLevelItemUnhovered()
{
	/*if (FrameImage)
	{
		FrameImage->SetVisibility( ESlateVisibility::Hidden);
	}*/
}