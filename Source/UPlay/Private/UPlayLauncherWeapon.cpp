// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayLauncherWeapon.h"
#include "UPlay/UplayProjectile.h"
#include "Math/Rotator.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Math/Vector.h"
#include "Math/Rotator.h"
#include "UPlayGrenadeProjectiler.h"

AUPlayLauncherWeapon::AUPlayLauncherWeapon()
{
	bReplicates = true;
}

void AUPlayLauncherWeapon::StartFire()
{

	if (CurrentAmmo.Bullets > 0)
	{
		MakeShot();
		
	}
	else
	{
		StopFire();
	}
		
}

void AUPlayLauncherWeapon::MakeShot()
{
	if (!GetWorld()) return;
		
	if (IsAmmoEmpty())
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), NoAmmoSound, GetActorLocation());
		return;
	}
	//FVector CameraLocation;
	//FRotator CameraRotation;
	//GetActorEyesViewPoint(CameraLocation, CameraRotation);

	//// Set MuzzleOffset to spawn projectiles slightly in front of the camera.
	//MuzzleOffsets.Set(0.0, 100.0f, 10.0f);

	//// Transform MuzzleOffset from camera space to world space.
	//FVector MuzzleLocation = CameraLocation + FTransform(CameraRotation).TransformVector(MuzzleOffsets);

	//// Skew the aim to be slightly upwards.
	//FRotator MuzzleRotation = CameraRotation;
	//MuzzleRotation.Pitch = 0.0f;

	//UWorld* World = GetWorld();
	//if (World)
	//{
	//	FActorSpawnParameters SpawnParams;
	//	SpawnParams.Owner = this;
	//	SpawnParams.Instigator = GetInstigator();

	//	const FTransform SpawnTransform(MuzzleRotation, GetMuzzleWorldLocation());

	//	//// Spawn the projectile at the muzzle.
	//	//AUPlayGrenadeProjectiler* Projectile = World->SpawnActor<AUPlayGrenadeProjectiler>(GrenadeProjectile, MuzzleLocation, MuzzleRotation, SpawnParams);
	//	////AUPlayGrenadeProjectiler* Projectile = GetWorld()->SpawnActorDeferred<AUPlayGrenadeProjectiler>(GrenadeProjectile, SpawnTransform); 
	//	if (Projectile)
	//	{
	//		// Set the projectile's initial trajectory.
	//		FVector LaunchDirection = MuzzleRotation.Vector();
	//		Projectile->SetOwner(GetOwner());
	//		Projectile->SetShotDirection(LaunchDirection);
	//	}


		FVector TraceStart, TraceEnd;
		if (!GetTraceData(TraceStart, TraceEnd)) return;

		FHitResult HitResult;
		FVector ViewLocation;
		FRotator ViewRotation;



		MakeHit(HitResult, TraceStart, TraceEnd);

		const FVector EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
		const FVector Direction = (EndPoint - GetMuzzleWorldLocation().GetSafeNormal());
	
		const FTransform SpawnTransform(GetMuzzleWorldRotation(), GetMuzzleWorldLocation());


		
		AUPlayGrenadeProjectiler* Projectile = GetWorld()->SpawnActorDeferred<AUPlayGrenadeProjectiler>(GrenadeProjectile, SpawnTransform);


		if(Projectile)
		{
			Projectile->SetShotDirection(Direction);
			Projectile->SetOwner(GetOwner());
			Projectile->FinishSpawning(SpawnTransform);
		}




	//}
	DecreaseAmmo();
	UGameplayStatics::SpawnSoundAttached(FireSound,
		WeaponMesh,
		MuzzleSocketName);
	UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionEffect, GetMuzzleWorldLocation(), FRotator::ZeroRotator, true, EPSCPoolMethod::AutoRelease);
	// 
	SpawnMuzzleFX();
}



	

