// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayHealthBarWidget.h"
#include "Components/ProgressBar.h"




void UUPlayHealthBarWidget::SetHealthPercent(float Percent)
{
	if (!HealthProgressBar) return;

	const auto HealthBarVisibility = (Percent > PercentVisibilityThreshold || FMath::IsNearlyZero(Percent) 
					? ESlateVisibility::Hidden 
					: ESlateVisibility::Visible);

	HealthProgressBar->SetVisibility(HealthBarVisibility);

	const auto HealthBarColor = Percent > PercentColorThreshold ? GoodColor : BadColor;
	HealthProgressBar->SetVisibility(HealthBarVisibility);
	HealthProgressBar->SetPercent(Percent);

}


