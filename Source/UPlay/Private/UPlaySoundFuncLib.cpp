// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlaySoundFuncLib.h"
#include "Sound/SoundClass.h"


DEFINE_LOG_CATEGORY_STATIC(LogUPlaySoundFuncLib, All, All)

void UUPlaySoundFuncLib::SetSoundClassVolume(USoundClass* SoundClass, float Volume)
{

	if (!SoundClass) return;

	SoundClass->Properties.Volume = FMath::Clamp(Volume, 0.0f, 1.0f);

	UE_LOG(LogUPlaySoundFuncLib, Display, TEXT("SoundClass Volume was changed: %s = %f"), *SoundClass->GetName(),
		SoundClass->Properties.Volume);
}

void UUPlaySoundFuncLib::ToggleSoundClassVolume(USoundClass* SoundClass)
{
	if (!SoundClass) return;

	const auto NextVolume = SoundClass->Properties.Volume > 0.0f ? 0.0f : 1.0f;
	SetSoundClassVolume(SoundClass, NextVolume);

}
