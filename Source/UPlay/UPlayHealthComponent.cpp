// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayHealthComponent.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Controller.h"
#include "GameFramework/Pawn.h"
#include "UPlay/UPlayGameModeBase.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Perception/AISense_Damage.h"
#include "GameFramework/Character.h"
#include "UPlayPlayerHUDWidget.h"


// Sets default values for this component's properties
UUPlayHealthComponent::UUPlayHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	
	
}


// Called when the game starts
void UUPlayHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	check(MaxHealth > 0)
		//Set Health Equal to Max Health.  

	//OnHealthChanged.Broadcast(Health);
	AActor* MyOwner = GetOwner();

	if (MyOwner)
	{
		//The Owner Object is now bound to respond to the OnTakeAnyDamage Function.        
		MyOwner->OnTakeAnyDamage.AddDynamic(this, &UUPlayHealthComponent::OnTakeAnyDamage);
	}
	         
	
}




void UUPlayHealthComponent::OnTakeAnyDamage(AActor* DamageActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f || IsDead())
	{
		OnDeath.Broadcast();
		return;
		
	}

	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth); 
	
	if (GetHealth() <= 0)
	{
		
		Killed(InstigatedBy);
		
		
	}
	
	if (IsDead())
	{
		OnDeath.Broadcast();
	}
	OnHealthChangedd.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);
	ReportDamageEvent(Damage, InstigatedBy);
	
}




void UUPlayHealthComponent::Killed(AController* KillerController)
{
	if (!GetWorld()) return;
	const auto GameMode = Cast<AUPlayGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GameMode) return;


	const auto Player = Cast<APawn>(GetOwner());
	const auto VictimController = Player ? Player->Controller : nullptr;

	GameMode->Killed(KillerController, VictimController);
	
}

// Called every frame
void UUPlayHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	
	
}

void UUPlayHealthComponent::UpdateHealth(float HealthChange)
{
	Health += HealthChange;
	Health = FMath::Clamp(Health, 0.0f, MaxHealth);
	
}

void UUPlayHealthComponent::SetHealth(float NewHealth)
{

	const auto NextHealth = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
	const auto HealthDelta = NextHealth - Health;

	Health = NextHealth; /*FMath::Clamp(NewHealth, 0.0f, MaxHealth);*/
	OnHealthChanged.Broadcast(Health, HealthDelta);
	
}




bool UUPlayHealthComponent::TryToAddHealth(float HealthAmount)
{
	if (Health == MaxHealth)
	{
		return false;
	}
	Health = Health + HealthAmount;
	return true;
}

bool UUPlayHealthComponent::ISHealthFull() const
{
	return FMath::IsNearlyEqual(Health, MaxHealth);
}

void UUPlayHealthComponent::ReportDamageEvent(float Damage, AController* InstigatedBy)
{

	if (!InstigatedBy || !InstigatedBy->GetPawn() || !GetOwner()) return;
	UAISense_Damage::ReportDamageEvent(GetWorld(),
		GetOwner(),
		InstigatedBy->GetPawn(),
		Damage, InstigatedBy->GetPawn()->GetActorLocation(),
		GetOwner()->GetActorLocation());


	
}
