// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayHealthPickableObject.h"
#include "UPlayHealthComponent.h"
#include "UPlayPickableObject.h"




	
void AUPlayHealthPickableObject::OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor)
{

	Super::OnOverlap(MyOverlappedActor, OtherActor);
	if (!MyCharacter) return;
 	if (MyCharacter->HealthComponent->GetHealth() <= 100.0f)
	{
		MyCharacter->HealthComponent->UpdateHealth(HealthAmount);			
	}	
}












