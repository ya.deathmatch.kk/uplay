// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Engine/Canvas.h" 
#include "UPLayCoreTypes.h"
#include "UPlayHUD.generated.h"

/**
 * 
 */

class UUPlayBaseWidget;


UCLASS()
class UPLAY_API AUPlayHUD : public AHUD
{
	GENERATED_BODY()

protected:
	// This will be drawn at the center of the screen.
	UPROPERTY(EditDefaultsOnly)
	UTexture2D* CrosshairTexture;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		TSubclassOf<UUserWidget> GameOverWidgetClass;


	virtual void BeginPlay() override;

public:

	// Primary draw call for the HUD.
	virtual void DrawHUD() override;

private:


	UPROPERTY()
		TMap <EUplayMatchState, UUPlayBaseWidget*> GameWidgets;

	void OnMatchStateChanged(EUplayMatchState State);

	UPROPERTY()
		UUPlayBaseWidget* CurrentWidget = nullptr;
};
