// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Public/UPlayPlayerController.h"
#include "Public/UPlayCoreTypes.h"
#include "UPlayGameModeBase.generated.h"






class AAIController;
UCLASS()
class UPLAY_API AUPlayGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:

	AUPlayGameModeBase();


	FOnMatchStateChangedSignature OnMatchStateChanged;


	virtual void StartPlay() override;
	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	TSubclassOf<AAIController> AIControllerClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
		TSubclassOf<APawn> AIPawnClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
		FGameData GameData;


	void Killed(AController* KillerController, AController* VictimController);
	
	FGameData GetGameData() const { return GameData; }
	int32 GetCurrentRoundsNum() const { return CurrentRound; }
	int32 GetRoundSecondsRemaining() const { return RoundCountDown; }


	void RespawnRequest(AController* Controller);


	virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
	virtual bool ClearPause() override;
private:

	EUplayMatchState MatchState = EUplayMatchState::WaitingToStart;

	FTimerHandle RespawnTimerHandle;

	int32 CurrentRound = 1;
	int32 RoundCountDown = 0;
	FTimerHandle GameRoundTimerHandle;

	void SpawnBots();
	void StartRound();
	void GameTimerUpdate();


	void ResetPlayers();
	void ResetOnePlayer(AController* Controller);

	void CreateTeamsInfo();
	FLinearColor DetermineColorByTeamID(int32 TeamID) const;
	void SetPlayerColor(AController* Controller);

	void LogPlayerInfo();

	void StartRespawn(AController* Controller);


	void GameOver();

	void SetMatchState(EUplayMatchState State);
};
