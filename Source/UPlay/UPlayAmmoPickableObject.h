// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPlayPickableObject.h"
#include "UPlayAmmoPickableObject.generated.h"

/**
 * 
 */ 


class AUPlayBaseWeapon;
UCLASS()
class UPLAY_API AUPlayAmmoPickableObject : public AUPlayPickableObject
{
	GENERATED_BODY()

	

protected:
	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp", meta = (ClampMin = "1.0", ClampMax = "10.0"))
	int32 ClipsAmount = 10;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp")
	TSubclassOf<AUPlayBaseWeapon> WeaponType;
	/*virtual void OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor) override;*/
	virtual bool GivePickUpTo(APawn* PlayerPawn) override;
	

	 

	 
};
