// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Public/UPlayCoreTypes.h"
#include "UPlayHealthComponent.generated.h"




DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FOnHealthChangedSignature, UUPlayHealthComponent*, HealthComponent, float, Health, float, DamageAmount, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, float, float)
DECLARE_MULTICAST_DELEGATE(FOnDeath)

class UAnimMontage;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UPLAY_API UUPlayHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUPlayHealthComponent();

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnHealthChangedSignature OnHealthChangedd;



	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamageActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	// Called when the game starts
	virtual void BeginPlay() override;
	
	FOnDeath OnDeath;
	FOnHealthChanged OnHealthChanged;

	float GetHealth() const{ return Health;};
	
	bool TryToAddHealth(float HealthAmount);
	bool ISHealthFull() const;

	UFUNCTION(BlueprintCallable)
	bool IsDead() const { return Health <= 0.0f; }

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetHealthPercent() const { return Health / MaxHealth; }

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Health", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
	float Health = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Health", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
	float MaxHealth = 100.0f;
protected:
	UPROPERTY(EditAnyWhere, Category = "Animation")
	UAnimMontage* DeathAnim;
	
private:
	

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void Killed(AController* KillerController);
	void UpdateHealth(float HealthChange);
	void SetHealth(float NewHealth);

	void ReportDamageEvent(float Damage, AController* InstigatedBy);
	
};
