// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/TextRenderComponent.h"
#include "UplayCharacter.generated.h"





class UUPlayHealthComponent;

class UUPlayWeaponComponent;
class USoundCue;
class UAnimMontage;




UCLASS()
class UPLAY_API AUplayCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUplayCharacter(const FObjectInitializer& ObjInit);
	
	FVector2D CameraInput;
	float ZoomFactor;
	bool bZoomingIn;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float LifeSpanOnDeath = 5.0f;

	//Setting Movement
	

	void ZoomIn();
	void ZoomOut();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetMovementDirection() const;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HealthComponent")
	UUPlayHealthComponent* HealthComponent;

	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponComponent")
	UUPlayWeaponComponent* WeaponComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* DeathSound;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Projectile class to spawn.
	//EditDefaultsOnly means that you will only be able to set the projectile class as a default on the Blueprint, not on each instance of the Blueprint.
	/*UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AUplayProjectile> ProjectileClass;*/

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Material")
		FName MaterialColorName = "Paint Color";

	UPROPERTY(EditAnyWhere,  Category = "Animation")
	UAnimMontage* DeathAnim;


	void OnHealthChanged(float Health, float HealthDelta);

		

public:	

	

	// Called every frame
	virtual void Tick(float DeltaTime) override;




	// Function that handles firing projectiles.
	/*UFUNCTION()
	void Fire();*/
	virtual void OnDeath();


	// Gun muzzle offset from the camera location.
	//EditAnywhere enables you to change the value of the muzzle offset within the Defaults mode of the Blueprint Editor or within the Details tab for any
	//  instance of the character. The BlueprintReadWrite specifier enables you to get and set the value of the muzzle offset within a Blueprint.
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector MuzzleOffset;*/

	void SetPlayerColor(const FLinearColor& Color);

};
