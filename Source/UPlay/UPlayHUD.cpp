// Fill out your copyright notice in the Description page of Project Settings.


#include "UPlayHUD.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"
#include "UPlayGameModeBase.h"
#include "UPlayBaseWidget.h"


DEFINE_LOG_CATEGORY_STATIC(LogUPlayHUD, All, All);

void AUPlayHUD::BeginPlay()
{
    Super::BeginPlay();

   /* auto PlayerHUDWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDWidgetClass);
    if (PlayerHUDWidget)
    {

        PlayerHUDWidget->AddToViewport();

    }*/
    GameWidgets.Add(EUplayMatchState::InProgress, CreateWidget<UUPlayBaseWidget>(GetWorld(), PlayerHUDWidgetClass));
    GameWidgets.Add(EUplayMatchState::Pause, CreateWidget<UUPlayBaseWidget>(GetWorld(), PauseWidgetClass));
    GameWidgets.Add(EUplayMatchState::GameOver, CreateWidget<UUPlayBaseWidget>(GetWorld(), GameOverWidgetClass));

    for (auto GameWidgetPair : GameWidgets)
    {
        const auto GameWidget = GameWidgetPair.Value;
        if (!GameWidget) continue;

        GameWidget->AddToViewport();
        GameWidget->SetVisibility(ESlateVisibility::Hidden);
    }



    if (GetWorld())
    {
        const auto GameMode = Cast<AUPlayGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &AUPlayHUD::OnMatchStateChanged);
        }
    }
}

void AUPlayHUD::DrawHUD()
{
    Super::DrawHUD();

    if (CrosshairTexture)
    {
        // Find the center of our canvas.
        FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

        // Offset by half of the texture's dimensions so that the center of the texture aligns with the center of the Canvas.
        FVector2D CrossHairDrawPosition(Center.X - (CrosshairTexture->GetSurfaceWidth() * 0.5f), Center.Y - (CrosshairTexture->GetSurfaceHeight() * 0.5f));

        // Draw the crosshair at the centerpoint.
        FCanvasTileItem TileItem(CrossHairDrawPosition, CrosshairTexture->Resource, FLinearColor::Green);
        TileItem.BlendMode = SE_BLEND_Translucent;
        Canvas->DrawItem(TileItem);
    }



}

void AUPlayHUD::OnMatchStateChanged(EUplayMatchState State)
{

    if (CurrentWidget)
    {
        CurrentWidget->SetVisibility(ESlateVisibility::Hidden);

    }
    if (GameWidgets.Contains(State))
    {
        CurrentWidget = GameWidgets[State];
    }
    if (CurrentWidget)
    {
        CurrentWidget->SetVisibility(ESlateVisibility::Visible);
        CurrentWidget->Show();

    }
    UE_LOG(LogUPlayHUD, Display, TEXT("Match State Changed: %s"), *UEnum::GetValueAsString(State));
     
}
