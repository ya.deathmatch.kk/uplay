// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "UPlayHealthComponent.h"
#include "UplayCharacter.h"
#include "UPlayPickableObject.generated.h"




class USphereComponent;
class USoundCue;

UCLASS()
class UPLAY_API AUPlayPickableObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUPlayPickableObject();
		

protected:
	// Called when the game starts or when spawned
	
	UPROPERTY(VisibleAnyWhere, Category = "Pickup")
	USphereComponent* CollisionComponent;

	

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Pickup")
	float RespawnTime = 5.0f;
	

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")
	USoundCue* PickupTakenSound;

	UPROPERTY(EditAnyWhere)
	AUplayCharacter* MyCharacter;

	virtual void BeginPlay() override;



	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	virtual void OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor);
	virtual void PickUpWasTaken();
	virtual void Respawn();
	virtual bool GivePickUpTo(APawn* PlayerPawn);
	bool CouldBeTaken() const;

	

private:
	FTimerHandle RespawnTimerHandle;
	float RotationYaw = 0.0f;
	void GenerateRotationYaw();
	
};
