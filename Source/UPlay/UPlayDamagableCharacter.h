// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/BoxComponent.h"
#include "UPlayHealthComponent.h"


#include "Components/TextRenderComponent.h"
#include "UPlayDamagableCharacter.generated.h"



class UStaticMeshComponent;
class UBoxComponent;
class UUPlayHealthComponent;




UCLASS()
class UPLAY_API AUPlsyDamagableCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUPlsyDamagableCharacter();


	UPROPERTY(EditAnyWhere, Category = "Mesh")
	UStaticMeshComponent* StaticMeshComponent;

	

	/*UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Damage")
	float Damage;*/

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HealthComponent")
	UUPlayHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component")
	UTextRenderComponent* HealthTextComponent;




protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	

	//UFUNCTION()
	//void OnHealthChanged(UUPlayHealthComponent* HealthComp, float Health, float DamageAmount, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	///*Function which will call damage to our Player*/
	//UFUNCTION()
	//void DamagePlayerCharacter();

	////Container for a Damage Type to inflict on the Player
	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	//TSubclassOf<UDamageType> GenericDamageType;


	//UFUNCTION()
	//void OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);


	UFUNCTION()
	void GetDamage( );
	

};
